import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

export default defineConfig({
  plugins: [react()],
  server: {
    open: true,
    port: 3001,
    watch: {
      usePolling: true,
    },
  },
  resolve: {
    alias: {
      scenes: "/src/scenes",
      components: "/src/components",
      services: "/src/services",
      theme: "/src/theme",
      constants: "/src/constants",
      utils: "/src/utils",
      config: "/src/config",
      store: "/src/store",
      layout: "/src/layout",
      hooks: "/src/hooks",
    },
  },
});
