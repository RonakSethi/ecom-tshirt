import { Route } from "react-router-dom";
import {
  AddCategory,
  AddProduct,
  Brands,
  CaptionTags,
  Categories,
  ClothLength,
  Colors,
  Discount,
  Fabric,
  Occasions,
  Orders,
  Patterns,
  PaymentModes,
  Products,
  Sections,
  SizeTypes,
  ViewCategory,
  ViewOrder,
  ViewProduct
} from "../scenes";
import Dashboard from "../scenes/dashboard";
import Profile from "../scenes/profile";

const ProtectedRoutes = () => {
  return (
    <>
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/categories" element={<Categories />} />
      <Route path="/categories/add" element={<AddCategory />} />
      <Route path="/categories/add/:id" element={<AddCategory />} />
      <Route path="/categories/:id" element={<ViewCategory />} />
      <Route path="/products" element={<Products />} />
      <Route path="/products/add" element={<AddProduct />} />
      <Route path="/products/add/:id" element={<AddProduct />} />
      <Route path="/products/:id" element={<ViewProduct />} />
      <Route path="/size-types" element={<SizeTypes />} />
      <Route path="/caption-tag" element={<CaptionTags />} />
      <Route path="/cloth-length" element={<ClothLength />} />
      <Route path="/colors" element={<Colors />} />
      <Route path="/discount" element={<Discount />} />
      <Route path="/fabric" element={<Fabric />} />
      <Route path="/occasions" element={<Occasions />} />
      <Route path="/patterns" element={<Patterns />} />
      <Route path="/brands" element={<Brands />} />
      <Route path="/sections" element={<Sections />} />
      <Route path="/payment-modes" element={<PaymentModes />} />
      <Route path="/orders" element={<Orders />} />
      <Route path="/orders/:id" element={<ViewOrder />} />
      <Route path="/profile" element={<Profile />} />
    </>
  );
};

export default ProtectedRoutes;
