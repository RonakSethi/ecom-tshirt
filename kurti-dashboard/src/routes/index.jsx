import { Route, Routes } from "react-router-dom";
import ProtectedLayout from "layout/ProtectedLayout";
import UnProtectedLayout from "layout/UnProtectedLayout";
import ProtectedRoutes from "./ProtectedRoutes";
import UnProtectedRoutes from "./UnProtectedRoutes";

const SiteRoutes = () => {
  return (
    <Routes>
      <Route path="/">
        <Route
          element={
            <UnProtectedLayout data={{ name: "KURTI", occupation: "" }} />
          }
        >
          {UnProtectedRoutes()}
        </Route>
        <Route
          element={<ProtectedLayout data={{ name: "KURTI", occupation: "" }} />}
        >
          {ProtectedRoutes()}
        </Route>
      </Route>
      <Route path="*" element={<center>404</center>} />
    </Routes>
  );
};

export default SiteRoutes;
