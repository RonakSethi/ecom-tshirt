import { Route } from "react-router-dom";
import { Signin } from "scenes/auth/Signin";

const UnProtectedRoutes = () => {
  return (
    <>
      <Route index path="login" element={<Signin />} />
      {/* <Route path="/forgotpassword" element={<Forgot />} /> */}
    </>
  );
};

export default UnProtectedRoutes;
