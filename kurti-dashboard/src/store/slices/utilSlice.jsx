import { createSlice } from "@reduxjs/toolkit";

const utilSlice = createSlice({
  name: "utils",
  initialState: {
    showMasterModal: false,
  },
  reducers: {
    setShowMasterModal: (state, action) => {
      state.showMasterModal = action.payload;
    },
  },
});

export const { setShowMasterModal } = utilSlice.actions;

export default utilSlice.reducer;
