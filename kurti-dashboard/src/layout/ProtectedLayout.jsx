import React, { useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import Sidebar from "./Sidebar";
import Topbar from "./Topbar";
import { useSelector } from "react-redux";

const ProtectedLayout = () => {
  const token = useSelector((state) => state.auth.token);
  const [isSidebar, setIsSidebar] = useState(true);

  if (!token) {
    return <Navigate to="/login" />;
  }

  return (
    <div className="app">
      <Sidebar isSidebar={isSidebar} />
      <main className="content">
        <Topbar setIsSidebar={setIsSidebar} />
        <Outlet />
      </main>
    </div>
  );
};

export default ProtectedLayout;
