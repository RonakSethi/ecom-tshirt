import { firebaseStorage } from "config/firebaseConfig";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";

export const handleFileUpload = async (
  file,
  folderPath,
  onUploadProgress,
  renderResult
) => {
  try {
    const storage = firebaseStorage;

    const storageRef = ref(storage, `${folderPath}/${file.name}`);

    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

        if (onUploadProgress) {
          onUploadProgress(progress);
        }
      },
      (error) => {
        console.error("Error uploading file:", error);
        throw error;
      },
      async () => {
        const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
        renderResult(downloadURL);
      }
    );
  } catch (error) {
    console.error("Error uploading file:", error);
    throw error;
  }
};
