import { API_ENDPOINTS } from "../constants/endpoints";
import { apiSlice, handleResponse } from "./base.service";

export const utilsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    deleteRow: builder.mutation({
      query: (body) => ({
        url: API_ENDPOINTS.DELETE_ROW,
        method: "DELETE",
        body,
      }),
      transformResponse: handleResponse,
      transformErrorResponse: handleResponse,
    }),
  }),
});

export const { useDeleteRowMutation } = utilsApiSlice;
