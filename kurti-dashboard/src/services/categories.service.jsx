import { API_ENDPOINTS } from "../constants/endpoints";
import { apiSlice, handleResponse } from "./base.service";

export const categoryApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getCategories: builder.query({
      query: (catId) => ({
        url: API_ENDPOINTS.GET_CATEGORIES,
        method: "GET",
        params: catId ? { catId } : {},
      }),
      transformResponse: handleResponse,
      transformErrorResponse: handleResponse,
    }),

    createCategory: builder.mutation({
      query: (body) => ({
        url: API_ENDPOINTS.CREATE_CATEGORY,
        method: "POST",
        body,
      }),
      transformResponse: handleResponse,
      transformErrorResponse: handleResponse,
    }),
  }),
});

export const { useCreateCategoryMutation, useGetCategoriesQuery } =
  categoryApiSlice;
