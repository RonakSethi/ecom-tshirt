import * as Yup from "yup";

// INITIAL VALUES

export const signInInitialValues = {
  email: "",
  password: "",
};

// VALIDATION SCHEMA

export const signInValidationSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email address")
    .required("Email is required"),
  password: Yup.string()
    .min(6, "Password should be of minimum 6 characters length")
    .required("Password is required"),
});
