import * as Yup from "yup";

// INITIAL VALUES

export const productInitialValues = {
  sku: "",
  name: "",
  categoryId: "",
  subCategoryId: "",
  description: "",
  additionalInformation: "",
  type: "B2C",
  brand: "",
  sections: [],
  occasion: [],
  fabric: [],
  length: "",
  pattern: "",
  color: [],
  tags: [],
  isSize: false,
  sizeType: [],
  mainImage: "",
  originalPrice: 0,
  sellingPrice: 0,
  discount: 0,
  discountLabel: "",
  b2bPrice: {},
  images: [],
  shopId: import.meta.env.VITE_SHOP_ID || "",
  shop: {
    name: import.meta.env.VITE_SHOP_NAME || "",
    location: import.meta.env.VITE_SHOP_LOCATION || "",
  },
  keywords: [],
  metaTitle: "",
  metaDescription: "",
  stock: [],
};

export const categoryInitialValues = {
  name: "",
  image: "",
  description: "",
};

// VALIDATION SCHEMA

export const categoryValidationSchema = Yup.object().shape({
  name: Yup.string().required("Color Name is required"),
  image: Yup.string().required("Image is required"),
  description: Yup.string().required("Description is required"),
});

export const productValidationSchema = Yup.object().shape({
  sku: Yup.string().required("SKU is required"),
  name: Yup.string().required("Name is required"),
  description: Yup.string().required("Description is required"),
  additionalInformation: Yup.string().required("Additional Information is required"),
  categoryId: Yup.string().required("Category is required"),
  brand: Yup.string().required("Brand is required"),
  mainImage: Yup.string().required("Main Image is required"),
  stock: Yup.array().min(1, "Stock is required"),
  originalPrice: Yup.number().min(1).required("Original Price is required"),
  sellingPrice: Yup.number().min(1).required("Selling Price is required"),
  discount: Yup.number().min(1).required("Discount is required"),
});
