export const API_ENDPOINTS = {
  // AUTH
  SIGN_IN: "/admin/log-in",

  // CATEGORIES
  GET_CATEGORIES: "/categories",
  CREATE_CATEGORY: "/categories",

  // PRODUCTS
  GET_PRODUCTS: "/products",
  CREATE_PRODUCT: "/products",

  // MASTERS
  GET_SIZE_TYPES: "/masters/size-type",
  GET_CAPTION_TAG: "/masters/caption-tag",
  GET_CLOTH_LENGTH: "/masters/cloth-length",
  GET_COLORS: "/masters/colors",
  GET_DISCOUNT: "/masters/discount",
  GET_FABRIC: "/masters/fabric",
  GET_OCCASIONS: "/masters/occasions",
  GET_PATTERNS: "/masters/patterns",
  GET_BRANDS: "/masters/brands",
  GET_SECTIONS: "/masters/sections",
  GET_PAYMENT_MODES: "/masters/payment-modes",
  ADD_SIZE_TYPE: "/masters/size-type",
  ADD_CAPTION_TAG: "/masters/caption-tag",
  ADD_CLOTH_LENGTH: "/masters/cloth-length",
  ADD_COLOR: "/masters/colors",
  ADD_DISCOUNT: "/masters/discount",
  ADD_FABRIC: "/masters/fabric",
  ADD_OCCASION: "/masters/occasions",
  ADD_PATTERN: "/masters/patterns",
  ADD_SECTION: "/masters/sections",
  ADD_SIZE_TYPE: "/masters/size-type",
  ADD_BRAND: "/masters/brands",
  ADD_PAYMENT_MODE: "/masters/payment-modes",

  // ORDERS
  GET_ORDERS: "/orders/all-orders",

  // UTILS
  DELETE_ROW: "/admin/delete-row",
};
