import EyeIcon from "@mui/icons-material/Visibility";
import { Box, Chip, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Header from "components/Header";
import { Link } from "react-router-dom";
import { useGetOrdersQuery } from "services";
import { tokens } from "theme";

const List = () => {
  // queries
  const { data: ordersData = [], isLoading } = useGetOrdersQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const columns = [
    {
      field: "orderId",
      headerName: "Order ID",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "Image",
      headerName: "Image",
      flex: 1,
      renderCell: (params) => (
        <img
          src={params.row?.products[0]?.productId?.mainImage}
          alt={params.row?.product?.name}
          style={{ width: "50px", height: "50px", objectFit: "contain" }}
        />
      ),
    },
    {
      field: "totalPrice",
      headerName: "Total Price",
      flex: 1,
      renderCell: (params) => <span>&#8377; {params.value}</span>,
    },
    {
      field: "status",
      headerName: "Status",
      flex: 1,
      renderCell: (params) => (
        <Chip
          label={params.value}
          color={params.value === "Delivered" ? "success" : "warning"}
          size="small"
          variant="outlined"
        />
      ),
    },
    {
      headerName: "Action",
      flex: 1,
      renderCell: (params) => (
        <Box display="flex" justifyContent="center">
          <Link to={`/orders/${params.row._id}`}>
            <EyeIcon />
          </Link>
        </Box>
      ),
    },
  ];

  return (
    <Box m="20px">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Header title="ORDERS" subtitle="Managing the Orders" />
      </Box>
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
        }}
      >
        <DataGrid
          rows={ordersData?.items || []}
          columns={columns}
          getRowId={(row) => row?._id}
          loading={isLoading}
        />
      </Box>
    </Box>
  );
};

export default List;
