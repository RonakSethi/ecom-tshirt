import { Box, Button, Typography, useTheme } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "components/Header";
import parse from "html-react-parser";
import { useNavigate, useParams } from "react-router-dom";
import { useGetCategoriesQuery } from "services";
import dataGridStyles from "../../styles/dataGridStyles";
import { GLOBAL_SETTINGS } from "constants/constantData";

const View = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const { id } = useParams();

  const { data: orderData } = useGetCategoriesQuery(id, {
    skip: !id,
  });

  const navigate = useNavigate();

  return (
    <Box m="20px">
      <Box sx={styles.mainHeadings}>
        <Header title="ORDER DETAILS" />

        <Box display="flex" justifyContent="end" mt="20px">
          <Button
            onClick={() => navigate("/orders")}
            color="secondary"
            variant="contained"
          >
            Back to Orders
          </Button>
        </Box>
      </Box>
      <Box sx={styles.formContainer}>
        <Box
          display="grid"
          gap="30px"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
          sx={{ "& > div": { gridColumn: isNonMobile ? undefined : "span 4" } }}
        >
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            OrderId:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {orderData?.orderId}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Status:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {orderData?.status}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Total Price:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {parse(GLOBAL_SETTINGS.CURRENCY)}
            {orderData?.totalPrice}
          </Typography>
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Payment Method:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {orderData?.paymentMethod}
          </Typography>
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
           Paid At:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {orderData?.paidAt}
          </Typography>
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Created At:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {orderData?.createdAt}
          </Typography>
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Shipping Address:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {orderData?.addressLine1}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default View;
