import { Box, Typography, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { tokens } from "theme";
import { mockDataTeam } from "../../data/mockData";
import AdminPanelSettingsOutlinedIcon from "@mui/icons-material/AdminPanelSettingsOutlined";
import LockOpenOutlinedIcon from "@mui/icons-material/LockOpenOutlined";
import SecurityOutlinedIcon from "@mui/icons-material/SecurityOutlined";
import Header from "components/Header";

const Team = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const columns = [
    { field: "id", headerName: "ID" },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "age",
      headerName: "Age",
      type: "number",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "phone",
      headerName: "Phone Number",
      flex: 1,
    },
    {
      field: "email",
      headerName: "Email",
      flex: 1,
    },
    {
      field: "accessLevel",
      headerName: "Access Level",
      flex: 1,
      renderCell: ({ row: { access } }) => {
        return (
          <Box
            width="60%"
            m="0 auto"
            p="5px"
            display="flex"
            justifyContent="center"
            backgroundColor={
              access === "admin"
                ? colors.blueAccent[600]
                : access === "manager"
                  ? colors.blueAccent[600]
                  : colors.blueAccent[600]
            }
            borderRadius="4px"
            color={colors.whiteAccent[100]}
          >
            {access === "admin" && <AdminPanelSettingsOutlinedIcon />}
            {access === "manager" && <SecurityOutlinedIcon />}
            {access === "user" && <LockOpenOutlinedIcon />}
            <Typography color={colors.whiteAccent[100]} sx={{ ml: "5px" }}>
              {access}
            </Typography>
          </Box>
        );
      },
    },
  ];

  return (
    <Box m="20px">
      <Header title="Team" subtitle="Managing the Team Members" />
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
            boxShadow: 'rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(15, 22, 36, 0.06) 0px 1px 2px 0px, rgba(15, 22, 36, 0.1) 0px 1px 3px 0px',
          },
          "& .MuiDataGrid-cell": {
            borderBottomColor: colors.whiteAccent[300],
            color: colors.whiteAccent[900],
            fontSize: 14,
            "&:focus": {
              outline: "none",
            }
          },
          "& .name-column--cell": {
            //color: colors.greenAccent[300],

          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.whiteAccent[100],
            borderBottomColor: colors.whiteAccent[200],
            fontSize: 14,
            fontWeight: 600,
            borderTopLeftRadius: "6px",
            borderTopRightRadius: "6px",
            "&:focus": {
              outline: "none",
              boxShadow: "none",
            }

          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.whiteAccent[100],
          },
          "& .MuiDataGrid-footerContainer": {
            backgroundColor: colors.whiteAccent[100],
            borderTop: "none",
            fontSize: 14,
            fontWeight: 600,
            borderBottomLeftRadius: "6px",
            borderBottomRightRadius: "6px",
          },
          "& .MuiCheckbox-root": {
            color: `${colors.whiteAccent[800]} !important`,
            //color: `#94A3B8  !important`,
          },
        }}
      >
        <DataGrid  rows={mockDataTeam} columns={columns} />
      </Box>
    </Box>
  );
};

export default Team;
