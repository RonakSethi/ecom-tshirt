import {
  Autocomplete,
  Box,
  Button,
  Chip,
  FormControlLabel,
  Modal,
  Switch,
  TextField,
  Typography,
  useTheme,
} from "@mui/material";
import { createFilterOptions } from "@mui/material/Autocomplete";
import useMediaQuery from "@mui/material/useMediaQuery";
import FileUploadProgress from "components/FileUploadProgress";
import Header from "components/Header";
import {
  productInitialValues,
  productValidationSchema,
} from "constants/schemas";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  useCreateCategoryMutation,
  useCreateProductMutation,
  useGetBrandsQuery,
  useGetCaptionTagQuery,
  useGetCategoriesQuery,
  useGetClothLengthQuery,
  useGetColorsQuery,
  useGetDiscountQuery,
  useGetFabricQuery,
  useGetOccasionsQuery,
  useGetPatternsQuery,
  useGetProductsQuery,
  useGetSectionsQuery,
  useGetSizeTypesQuery,
} from "services";
import { handleFileUpload } from "utils/common";
import dataGridStyles from "../../styles/dataGridStyles";

const Add = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");

  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);

  // hooks
  const navigate = useNavigate();
  const { id: productId } = useParams();

  // state
  const [uploadProgress, setUploadProgress] = useState({
    mainImage: 0,
    images: 0,
  });
  const [show, setShow] = useState(false);

  // mutation
  const [createProduct] = useCreateProductMutation();

  // query
  const { data: productData } = useGetProductsQuery(productId, {
    skip: !productId,
  });

  const getPreFilledValues = () => {
    if (productData?._id) {
      const {
        name,
        sku,
        description,
        additionalInformation,
        seo,
        attributes,
        prices,
        stock,
        images,
        mainImage,
        categoryId,
      } = productData;
      const {
        brand,
        sections,
        occasion,
        fabric,
        length,
        pattern,
        color,
        tags,
        discount: discountLabel,
      } = attributes;
      const { keywords, metaTitle, metaDescription } = seo;
      const { originalPrice, sellingPrice, discount } = prices?.b2cPrice || {};
      return {
        name,
        sku,
        description,
        additionalInformation,
        brand,
        sections,
        occasion,
        fabric,
        length,
        pattern,
        color,
        tags,
        discountLabel,
        originalPrice,
        sellingPrice,
        keywords,
        metaTitle,
        metaDescription,
        stock,
        images,
        mainImage,
        discount,
        categoryId: categoryId?._id,
      };
    }
    return productInitialValues;
  };

  const formik = useFormik({
    initialValues: getPreFilledValues() || productInitialValues,
    enableReinitialize: true,
    validationSchema: productValidationSchema,
    onSubmit: (values) => {
      const {
        brand,
        sections,
        occasion,
        fabric,
        length,
        pattern,
        color,
        tags,
        keywords,
        metaTitle,
        metaDescription,
        originalPrice,
        sellingPrice,
        discount,
        discountLabel,
        ...rest
      } = values;
      const payload = {
        ...rest,
        attributes: {
          brand,
          sections,
          occasion,
          fabric,
          length,
          pattern,
          color,
          tags,
          discount: discountLabel,
        },
        seo: {
          keywords,
          metaTitle,
          metaDescription,
        },
        b2cPrice: {
          originalPrice,
          sellingPrice,
          discount,
        },
      };
      createProduct(payload)
        .unwrap()
        .then((result) => {
          if (result?._id) {
            navigate("/products");
          }
        });
    },
  });

  // query
  const { data: categories } = useGetCategoriesQuery();
  const { data: subCategories } = useGetCategoriesQuery(
    formik.values.categoryId,
    {
      skip: !formik.values.categoryId,
    }
  );
  const { data: brands } = useGetBrandsQuery();
  const { data: sections = [] } = useGetSectionsQuery();
  const { data: ocassions = [] } = useGetOccasionsQuery();
  const { data: fabrics = [] } = useGetFabricQuery();
  const { data: lengths = [] } = useGetClothLengthQuery();
  const { data: colors = [] } = useGetColorsQuery();
  const { data: patterns = [] } = useGetPatternsQuery();
  const { data: captions = [] } = useGetCaptionTagQuery();
  const { data: sizeTypes = [] } = useGetSizeTypesQuery();
  const { data: discountData = [] } = useGetDiscountQuery();

  const handleFileChange = (evt, type) => {
    const file = evt.target.files[0];
    const fileName = `${Date.now()}_${file.name}`;
    const newFile = new File([file], fileName, { type: file.type });
    if (newFile) {
      handleFileUpload(
        newFile,
        "products",
        (progress) => {
          setUploadProgress({
            ...uploadProgress,
            [type]: progress,
          });
        },
        (url) => {
          if (type === "mainImage" && url) {
            formik.setFieldValue("mainImage", url || "");
          } else if (type === "images" && url) {
            formik.setFieldValue("images", [...formik.values.images, url]);
          }
        }
      );
    }
  };

  const filter = createFilterOptions();

  const handleChange = (event, newValue) => {
    const updatedNewValue = newValue.map((item) => {
      if (typeof item === "string") {
        return item;
      }
      if (item.inputValue) {
        return item.inputValue;
      }
      return item;
    });

    const uniqueValues = Array.from(new Set(updatedNewValue));
    formik.setFieldValue("keywords", uniqueValues);
  };

  const tableStyle = {
    width: "100%",
    borderCollapse: "collapse",
    margin: "25px 0",
    fontSize: "18px",
    textAlign: "left",
  };

  const thTdStyle = {
    padding: "12px",
    borderBottom: "1px solid #ddd",
  };

  const handleRemove = (index) => {
    const newStock = formik.values.stock.filter((_, i) => i !== index);
    formik.setFieldValue("stock", newStock);
  };

  return (
    <Box m="20px">
      <Box sx={styles.mainHeadings}>
        <Header
          title={`${productId ? "EDIT" : "CREATE"} PRODUCT`}
          subtitle={`${productId ? "Edit Product" : "Create a New Product"}`}
        />
      </Box>
      <form onSubmit={formik.handleSubmit}>
        <Box
          sx={{
            ...styles.formContainer,
          }}
        >
          <Box
            display="grid"
            gap="30px"
            gridTemplateColumns="repeat(4, minmax(0, 1fr))"
            sx={{
              "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
            }}
          >
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="SKU ID"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.sku}
              name="sku"
              error={!!formik.touched.sku && !!formik.errors.sku}
              helperText={formik.touched.sku && formik.errors.sku}
              sx={{ gridColumn: "span 2" }}
              className="input"
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Name"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.name}
              name="name"
              error={!!formik.touched.name && !!formik.errors.name}
              helperText={formik.touched.name && formik.errors.name}
              sx={{ gridColumn: "span 2" }}
              className="input"
            />
            <TextField
              fullWidth
              variant="filled"
              type="number"
              label="Price"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.originalPrice}
              name="originalPrice"
              error={
                !!formik.touched.originalPrice && !!formik.errors.originalPrice
              }
              helperText={
                formik.touched.originalPrice && formik.errors.originalPrice
              }
              sx={{ gridColumn: "span 2" }}
              InputLabelProps={{ shrink: true }}
              className="input"
            />
            <TextField
              fullWidth
              variant="filled"
              type="number"
              label="Selling Price"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.sellingPrice}
              name="sellingPrice"
              error={
                !!formik.touched.sellingPrice && !!formik.errors.sellingPrice
              }
              helperText={
                formik.touched.sellingPrice && formik.errors.sellingPrice
              }
              sx={{ gridColumn: "span 2" }}
              InputLabelProps={{ shrink: true }}
              className="input"
            />
            <TextField
              fullWidth
              variant="filled"
              type="number"
              label="Discount"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.discount}
              name="discount"
              error={!!formik.touched.discount && !!formik.errors.discount}
              helperText={formik.touched.discount && formik.errors.discount}
              sx={{ gridColumn: "span 2" }}
              InputLabelProps={{ shrink: true }}
              className="input"
            />
            <Autocomplete
              options={brands}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              onChange={(_, value) =>
                formik.setFieldValue("brand", value?.name)
              }
              name="brand"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Select Brand"
                  error={!!formik.touched.brand && !!formik.errors.brand}
                  helperText={formik.touched.brand && formik.errors.brand}
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={
                brands?.find((item) => item.name === formik.values.brand) ||
                null
              }
            />
            <Autocomplete
              options={categories}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              onChange={(_, value) =>
                formik.setFieldValue("categoryId", value?._id)
              }
              name="categoryId"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Select Category"
                  error={
                    !!formik.touched.categoryId && !!formik.errors.categoryId
                  }
                  helperText={
                    formik.touched.categoryId && formik.errors.categoryId
                  }
                />
              )}
              value={categories?.find(
                (category) => category?._id === formik.values.categoryId
              )}
            />
            {subCategories?.length > 0 && (
              <Autocomplete
                options={subCategories}
                getOptionLabel={(option) => option.name}
                onBlur={formik.handleBlur}
                onChange={(_, value) =>
                  formik.setFieldValue("subCategoryId", value?._id)
                }
                className="select"
                name="subCategoryId"
                sx={{ gridColumn: "span 2" }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    fullWidth
                    variant="filled"
                    label="Select Sub Category"
                  />
                )}
              />
            )}
            <Autocomplete
              options={sections}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              multiple
              onChange={(_, value) => {
                const sectionNames = value.map((item) => item.name);
                formik.setFieldValue("sections", sectionNames);
              }}
              name="sections"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Home Page Section"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={sections?.filter((item) =>
                formik.values.sections.includes(item.name)
              )}
            />
            <Autocomplete
              options={discountData}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              onChange={(_, value) => {
                formik.setFieldValue("discountLabel", value?.name);
              }}
              name="discountLabel"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Discount Label"
                />
              )}
              value={discountData?.find(
                (item) => item.name === formik.values.discountLabel
              )}
            />
            <Autocomplete
              options={ocassions}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              multiple
              onChange={(_, value) => {
                const occasionNames = value.map((item) => item.name);
                formik.setFieldValue("occasion", occasionNames);
              }}
              name="occasion"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Occasion"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={ocassions?.filter((item) =>
                formik.values.occasion.includes(item.name)
              )}
            />
            <Autocomplete
              options={fabrics}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              multiple
              onChange={(_, value) => {
                const fabricNames = value.map((item) => item.name);
                formik.setFieldValue("fabric", fabricNames);
              }}
              name="fabric"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Fabric"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={fabrics?.filter((item) =>
                formik.values.fabric.includes(item.name)
              )}
            />
            <Autocomplete
              options={lengths}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              onChange={(_, value) =>
                formik.setFieldValue("length", value?.name)
              }
              name="length"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Length"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={lengths?.find(
                (item) => item.name === formik.values.length
              )}
            />
            <Autocomplete
              options={colors}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              multiple
              onChange={(_, value) => {
                const colorNames = value.map(({ name, hexCode }) => ({
                  name,
                  hexCode,
                }));
                formik.setFieldValue("color", colorNames);
              }}
              name="color"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Color"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={colors?.filter((item) =>
                formik.values.color.some((color) => color.name === item.name)
              )}
            />
            <Autocomplete
              options={patterns}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              onChange={(_, value) =>
                formik.setFieldValue("pattern", value?.name)
              }
              name="pattern"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Pattern"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={patterns?.find(
                (item) => item.name === formik.values.pattern
              )}
            />
            <Autocomplete
              options={captions}
              getOptionLabel={(option) => option.name}
              onBlur={formik.handleBlur}
              multiple
              onChange={(_, value) => {
                const tagNames = value.map((item) => item.name);
                formik.setFieldValue("tags", tagNames);
              }}
              name="tags"
              sx={{ gridColumn: "span 2" }}
              className="select"
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="filled"
                  label="Tag/Label"
                  InputLabelProps={{ shrink: true }}
                />
              )}
              value={captions?.filter((item) =>
                formik.values.tags.includes(item.name)
              )}
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Description"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.description}
              name="description"
              error={
                !!formik.touched.description && !!formik.errors.description
              }
              helperText={
                formik.touched.description && formik.errors.description
              }
              sx={{ gridColumn: "span 4" }}
              className="input"
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Additional Information"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.additionalInformation}
              name="additionalInformation"
              className="input"
              error={
                !!formik.touched.additionalInformation &&
                !!formik.errors.additionalInformation
              }
              helperText={
                formik.touched.additionalInformation &&
                formik.errors.additionalInformation
              }
              sx={{ gridColumn: "span 4" }}
            />
            <FormControlLabel
              control={
                <Switch
                  checked={formik.values.isSize}
                  onChange={formik.handleChange}
                  name="isSize"
                />
              }
              label="Size Available"
              sx={{ gridColumn: formik.values.isSize ? "span 2" : "span 4" }}
            />
            {formik.values.isSize && (
              <Autocomplete
                options={sizeTypes}
                getOptionLabel={(option) => option.name}
                onBlur={formik.handleBlur}
                onChange={(_, value) =>
                  formik.setFieldValue("sizeType", value?.values)
                }
                name="sizeType"
                sx={{ gridColumn: "span 2" }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    fullWidth
                    variant="filled"
                    label="Size Type"
                  />
                )}
                className="select"
              />
            )}
            <TextField
              fullWidth
              variant="filled"
              type="file"
              onBlur={formik.handleBlur}
              onChange={(evt) => handleFileChange(evt, "mainImage")}
              name="mainImage"
              error={!!formik.errors.mainImage}
              helperText={formik.errors.mainImage}
              sx={{ gridColumn: "span 2" }}
              className="input"
              InputProps={{
                accept: "image/*",
                endAdornment: (
                  <FileUploadProgress progress={uploadProgress?.mainImage} />
                ),
              }}
              InputLabelProps={{ shrink: true }}
              label="Main Image"
            />
            <TextField
              fullWidth
              variant="filled"
              type="file"
              onBlur={formik.handleBlur}
              onChange={(evt) => handleFileChange(evt, "images")}
              name="image"
              error={!!formik.errors.images}
              helperText={formik.errors.images}
              sx={{ gridColumn: "span 2" }}
              className="input"
              InputProps={{
                accept: "image/*",
                endAdornment: (
                  <FileUploadProgress progress={uploadProgress?.images} />
                ),
              }}
              InputLabelProps={{ shrink: true }}
              label="Images"
            />
            <Box
              sx={{
                gridColumn: "span 4",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <h3 style={{ margin: 0 }}>Stocks</h3>
              <Button
                color="secondary"
                variant="contained"
                onClick={() => setShow(true)}
                sx={styles.buttonMd}
              >
                Add Stock
              </Button>
            </Box>
            {formik.errors.stock && (
              <Box sx={{ gridColumn: "span 4" }}>
                <Typography color="error">{formik.errors.stock}</Typography>
              </Box>
            )}
            {formik.values.stock?.length > 0 && (
              <Box sx={{ gridColumn: "span 4" }}>
                <table style={tableStyle}>
                  <thead>
                    <tr>
                      <th style={thTdStyle}>Size Type</th>
                      <th style={thTdStyle}>Quantity</th>
                      <th style={thTdStyle}>Unit Price</th>
                      <th style={thTdStyle}>Selling Price</th>
                      <th style={thTdStyle}>Product Type</th>
                      <th style={thTdStyle}>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {formik.values.stock.map((item, index) => (
                      <tr key={index}>
                        <td style={thTdStyle}>{item.sizeType}</td>
                        <td style={thTdStyle}>{item.minqty}</td>
                        <td style={thTdStyle}>{item.unitPrice}</td>
                        <td style={thTdStyle}>{item.sellingPrice}</td>
                        <td style={thTdStyle}>{item.productType}</td>
                        <td style={thTdStyle}>
                          <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => handleRemove(index)}
                          >
                            Remove
                          </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </Box>
            )}

            <h3>SEO Information</h3>
            <br />
            <Autocomplete
              fullWidth
              multiple
              value={formik.values.keywords}
              onChange={handleChange}
              filterOptions={(options, params) => {
                const filtered = filter(options, params);

                if (params.inputValue !== "") {
                  filtered.push(params.inputValue);
                }

                return filtered;
              }}
              options={formik.values.keywords}
              getOptionLabel={(option) => option}
              renderOption={(props, option) => <li {...props}>{option}</li>}
              freeSolo
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip
                    variant="outlined"
                    label={option}
                    {...getTagProps({ index })}
                  />
                ))
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  label="Keywords"
                  placeholder="Create options"
                  error={
                    formik.touched.keywords && Boolean(formik.errors.keywords)
                  }
                />
              )}
              className="select-lg"
              sx={{ gridColumn: "span 4" }}
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Meta Title"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.metaTitle}
              name="metaTitle"
              error={!!formik.touched.metaTitle && !!formik.errors.metaTitle}
              helperText={formik.touched.metaTitle && formik.errors.metaTitle}
              sx={{ gridColumn: "span 4" }}
              className="input"
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Meta Description"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.metaDescription}
              name="metaDescription"
              error={
                !!formik.touched.metaDescription &&
                !!formik.errors.metaDescription
              }
              helperText={
                formik.touched.metaDescription && formik.errors.metaDescription
              }
              sx={{ gridColumn: "span 4" }}
              className="input"
            />
          </Box>
          <Box display="flex" justifyContent="end" mt="20px">
            <Button
              type="submit"
              color="secondary"
              variant="contained"
              sx={styles.buttonMd}
            >
              Create
            </Button>
          </Box>
        </Box>
      </form>
      <AddStockModal
        formik={formik}
        sizeTypes={sizeTypes}
        show={show}
        setShow={setShow}
      />
    </Box>
  );
};

export default Add;

const AddStockModal = ({ formik, sizeTypes, show, setShow }) => {
  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const [stockObj, setStockObj] = useState({
    sizeType: [],
    minqty: 0,
    unitPrice: 0,
    sellingPrice: 0,
    productType: "B2C",
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    formik.setFieldValue("stock", [...formik.values.stock, stockObj]);
    setShow(false);
    setStockObj({
      sizeType: [],
      minqty: 0,
      unitPrice: 0,
      sellingPrice: 0,
      productType: "B2C",
    });
  };

  useEffect(() => {
    if (show) {
      setStockObj({
        ...stockObj,
        unitPrice: formik.values.originalPrice,
        sellingPrice: formik.values.sellingPrice,
      });
    }
  }, [show]);

  return (
    <Modal open={show} onClose={() => setShow(false)}>
      <Box sx={{ ...styles.formContainer, ...styles.modalContainer }}>
        <Typography variant="h6">Add Stock</Typography>
        <form onSubmit={handleSubmit}>
          <Autocomplete
            options={formik.values.isSize ? formik.values.sizeType : []}
            getOptionLabel={(option) => option}
            onBlur={formik.handleBlur}
            onChange={(_, value) =>
              setStockObj({ ...stockObj, sizeType: value })
            }
            name="sizeType"
            sx={{ gridColumn: "span 2" }}
            renderInput={(params) => (
              <TextField
                {...params}
                fullWidth
                variant="filled"
                label="Size Type"
              />
            )}
            className="select"
          />
          <TextField
            label="Quantity"
            variant="outlined"
            fullWidth
            margin="normal"
            type="number"
            value={stockObj.minqty || 0}
            onChange={(e) =>
              setStockObj({ ...stockObj, minqty: e.target.value })
            }
            className="input fieldset-1"
          />
          <TextField
            label="Original Price"
            variant="outlined"
            type="number"
            fullWidth
            margin="normal"
            value={stockObj.unitPrice || 0}
            onChange={(e) =>
              setStockObj({ ...stockObj, unitPrice: e.target.value })
            }
            className="input fieldset-1"
          />
          <TextField
            label="Selling Price"
            variant="outlined"
            fullWidth
            type="number"
            margin="normal"
            value={stockObj.sellingPrice || 0}
            onChange={(e) =>
              setStockObj({ ...stockObj, sellingPrice: e.target.value })
            }
            className="input fieldset-1"
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={{ mt: 2 }}
          >
            Submit
          </Button>
          <Button
            variant="contained"
            color="secondary"
            sx={{ mt: 2, ml: 2 }}
            onClick={() => setShow(false)}
          >
            Close
          </Button>
        </form>
      </Box>
    </Modal>
  );
};
