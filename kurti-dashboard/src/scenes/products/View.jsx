import { Box, Button, Typography, useTheme } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "components/Header";
import { GLOBAL_SETTINGS } from "constants/constantData";
import parse from "html-react-parser";
import { useNavigate, useParams } from "react-router-dom";
import { useGetProductsQuery } from "services";
import dataGridStyles from "../../styles/dataGridStyles";

const View = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const { id } = useParams();

  const { data: productData } = useGetProductsQuery(id, {
    skip: !id,
  });

  const navigate = useNavigate();

  return (
    <Box m="20px">
      <Box sx={styles.mainHeadings}>
        <Header title="PRODUCT DETAILS" />

        <Box display="flex" justifyContent="end" mt="20px">
          <Button
            onClick={() => navigate("/products")}
            color="secondary"
            variant="contained"
          >
            Back to Products
          </Button>
        </Box>
      </Box>
      <Box sx={styles.formContainer}>
        <Box
          display="grid"
          gap="30px"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
          sx={{ "& > div": { gridColumn: isNonMobile ? undefined : "span 4" } }}
        >
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            SKU ID:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.sku}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Name:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.name}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Price:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {parse(GLOBAL_SETTINGS?.CURRENCY)}
            {productData?.prices?.b2cPrice?.originalPrice}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Selling Price:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {parse(GLOBAL_SETTINGS?.CURRENCY)}
            {productData?.prices?.b2cPrice?.sellingPrice}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Discount:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.prices?.b2cPrice?.discount}%
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Shop Name:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.shop?.name}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Shop Address:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.shop?.location}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Brand:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.brand}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Category:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.categoryId?.name}
          </Typography>

          {/* {productData?.subCategory && (
            <>
              <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
                Sub Category:
              </Typography>
              <Typography sx={{ gridColumn: "span 1" }}>
                {productData?.subCategory.name}
              </Typography>
            </>
          )} */}

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Sections:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.sections?.join(", ")}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Discount Label:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.discount}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Occasion:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.occasion?.join(", ")}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Fabric:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.fabric?.join(", ")}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Length:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.length}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Color:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.color?.map((col) => col.name).join(", ")}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Pattern:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.pattern}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Tags:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.attributes?.tags?.join(", ")}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Size Available:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.isSize ? "Yes" : "No"}
          </Typography>

          {productData?.isSize && (
            <>
              <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
                Size Type:
              </Typography>
              <Typography sx={{ gridColumn: "span 1" }}>
                {productData?.sizeType?.length > 0
                  ? productData?.sizeType.join(", ")
                  : "All"}
              </Typography>
            </>
          )}

          <Box sx={{ gridColumn: "span 4" }}>
            <Typography variant="h6">Description:</Typography>
            <Typography>{parse(productData?.description ?? '')}</Typography>
          </Box>

          <Box sx={{ gridColumn: "span 4" }}>
            <Typography variant="h6">Additional Information:</Typography>
            <Typography>{parse(productData?.additionalInformation ?? '')}</Typography>
          </Box>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Keywords:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.seo?.keywords.join(", ")}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Meta Title:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.seo?.metaTitle}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Meta Description:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {productData?.seo?.metaDescription}
          </Typography>

          <Box sx={{ gridColumn: "span 4" }}>
            <Typography variant="h6">Stocks:</Typography>
            <table
              style={{
                width: "100%",
                borderCollapse: "collapse",
                margin: "25px 0",
                fontSize: "18px",
                textAlign: "left",
              }}
            >
              <thead>
                <tr>
                  <th
                    style={{ padding: "12px", borderBottom: "1px solid #ddd" }}
                  >
                    Size Type
                  </th>
                  <th
                    style={{ padding: "12px", borderBottom: "1px solid #ddd" }}
                  >
                    Quantity
                  </th>
                  <th
                    style={{ padding: "12px", borderBottom: "1px solid #ddd" }}
                  >
                    Unit Price
                  </th>
                  <th
                    style={{ padding: "12px", borderBottom: "1px solid #ddd" }}
                  >
                    Selling Price
                  </th>
                  <th
                    style={{ padding: "12px", borderBottom: "1px solid #ddd" }}
                  >
                    Product Type
                  </th>
                </tr>
              </thead>
              <tbody>
                {productData?.stock.map((item, index) => (
                  <tr key={index}>
                    <td
                      style={{
                        padding: "12px",
                        borderBottom: "1px solid #ddd",
                      }}
                    >
                      {item?.sizeType}
                    </td>
                    <td
                      style={{
                        padding: "12px",
                        borderBottom: "1px solid #ddd",
                      }}
                    >
                      {item?.minqty}
                    </td>
                    <td
                      style={{
                        padding: "12px",
                        borderBottom: "1px solid #ddd",
                      }}
                    >
                      {item?.unitPrice}
                    </td>
                    <td
                      style={{
                        padding: "12px",
                        borderBottom: "1px solid #ddd",
                      }}
                    >
                      {item?.sellingPrice}
                    </td>
                    <td
                      style={{
                        padding: "12px",
                        borderBottom: "1px solid #ddd",
                      }}
                    >
                      {item?.productType}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Box>

          <Box sx={{ gridColumn: "span 4", marginTop: "30px" }}>
            <Typography variant="h6">Main Image:</Typography>
            <img
              src={productData?.mainImage}
              alt="Main"
            />
          </Box>

          <Box sx={{ gridColumn: "span 4", marginTop: "30px" }}>
            <Typography variant="h6">Images:</Typography>
            <Box display="flex" flexWrap="wrap" gap="10px">
              {productData?.images.map((image, index) => (
                <img
                  key={index}
                  src={image}
                  alt={`Image ${index + 1}`}
                  style={{ width: "100px", height: "auto" }}
                />
              ))}
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default View;
