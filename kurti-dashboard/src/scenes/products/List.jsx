import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import EyeIcon from "@mui/icons-material/Visibility";
import { Box, Button, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Header from "components/Header";
import { useDeleteRecord } from "hooks";
import { Link, useNavigate } from "react-router-dom";
import { useGetProductsQuery } from "services";
import { tokens } from "theme";
import dataGridStyles from "../../styles/dataGridStyles";

const List = () => {
  // queries
  const { data = {}, isLoading, refetch } = useGetProductsQuery();
  const { items: productsData = [] } = data ?? {};

  // hooks
  const { deleteRecord } = useDeleteRecord();

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const styles = dataGridStyles(theme.palette.mode);
  const navigate = useNavigate();

  const columns = [
    {
      field: "sku",
      headerName: "SKU ID",
      flex: 1,
    },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "mainImage",
      headerName: "Image",
      flex: 1,
      renderCell: (params) => (
        <img
          src={params.value}
          alt={params.row.name}
          style={{ width: "50px", height: "50px", objectFit: "contain" }}
        />
      ),
    },
    {
      field: "prices.b2cPrice.sellingPrice",
      headerName: "Price",
      flex: 1,
      renderCell: (params) => {
        return <>&#8377;{params.row.prices.b2cPrice.sellingPrice}</>;
      },
    },
    {
      headerName: "Action",
      flex: 1,
      renderCell: (params) => (
        <Box display="flex" justifyContent="center" gap={2}>
          <Link to={`/products/add/${params.row._id}`}>
            <EditIcon />
          </Link>
          <span
            className="cursor-pointer"
            onClick={() => {
              const payload = {
                modelName: "Product",
                status: true,
                modelId: params.row._id,
              };
              deleteRecord(payload, refetch);
            }}
          >
            <DeleteIcon />
          </span>
          <Link to={`/products/${params.row._id}`}>
            <EyeIcon />
          </Link>
        </Box>
      ),
    },
  ];

  return (
    <Box m="20px">
      <Box
        sx={{
          ...styles.headingsContainer,
          ...styles.dFlex,
          ...styles.alignItemsStart,
          ...styles.justifyContentBetween,
        }}
      >
        <Box sx={styles.mainHeadings}>
          <Header title="PRODUCTS" subtitle="Managing the Products" />
        </Box>
        <Box>
          <Button sx={styles.buttonMd} LinkComponent={Link} to="/products/add">
            <AddOutlinedIcon sx={{ mr: "10px" }} />
            Add Product
          </Button>
        </Box>
      </Box>
      <Box
        m="0px 0 0 0"
        height="75vh"
        sx={{
          ...styles.listContainer,
        }}
      >
        <DataGrid
          rows={productsData}
          columns={columns}
          getRowId={(row) => row._id}
          loading={isLoading}
        />
      </Box>
    </Box>
  );
};

export default List;
