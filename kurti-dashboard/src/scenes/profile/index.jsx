import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import { Box, Button, Grid, useTheme, TextField } from '@mui/material'
import React from 'react'
import Header from "components/Header";
import { tokens } from "theme";
import { Link } from "react-router-dom";
import dataGridStyles from "../../styles/dataGridStyles";
import FileUploadProgress from "components/FileUploadProgress";
import profileImge from '../../assets/14.jpg';

const Profile = () => {

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const styles = dataGridStyles(theme.palette.mode);

  return (
    <Box m="20px">
      <Box sx={{ ...styles.headingsContainer, ...styles.dFlex, ...styles.alignItemsStart, ...styles.justifyContentBetween, }}>
        <Box sx={styles.mainHeadings}>
          <Header title="Profile" subtitle="Manage your profile" />
        </Box>

      </Box>

      <Box sx={{ ...styles.boxContainer, ...styles.formContainer, }}>
        <Box sx={{ display: "flex", alignItems: "flex-start", justifyContent: "flex-start", columnGap: "30px" }}>
          <Box className="profile-image-box" gap="30px">
            <img src={profileImge} alt="" />
          </Box>
          <Box sx={{ maxWidth: "400px", minWidth: "400px",  }}>
            <TextField
              fullWidth
              variant="filled"
              type="file"


              name="mainImage"

              sx={{ gridColumn: "span 4" }}
              className="input"
              InputProps={{
                accept: "image/*",
                endAdornment: (
                  <FileUploadProgress progress={"uploadProgress?.mainImage"} />
                ),
              }}
              InputLabelProps={{ shrink: true }}
              label="Profile Image"
            />
          </Box>
        </Box>
        <Box
          display="grid"
          gap="30px"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >

          <TextField
            fullWidth
            variant="filled"
            type="text"
            label="First Name"
            name="sku"
            value={""}
            sx={{ gridColumn: "span 2" }}
            className="input"
          />

          <TextField
            fullWidth
            variant="filled"
            type="text"
            label="Last Name"
            name="sku"
            value={""}
            sx={{ gridColumn: "span 2" }}
            className="input"
          />
          <TextField
            fullWidth
            variant="filled"
            type="text"
            label="Username"
            name="sku"
            value={""}
            sx={{ gridColumn: "span 2" }}
            className="input"
          />
          <TextField
            fullWidth
            variant="filled"
            type="text"
            label="Phonenumber"
            name="sku"
            value={""}
            sx={{ gridColumn: "span 2" }}
            className="input"
          />
          <TextField
            fullWidth
            variant="filled"
            type="text"
            label="Email"
            name="sku"
            value={""}
            sx={{ gridColumn: "span 2" }}
            className="input"
          />
          <Box sx={{ textAlign: "right", gridColumn: "span 2" }}>
            <Button
              color="secondary"
              variant="contained"
              onClick={() => setShow(true)}
              sx={styles.buttonMd}
            >
              Save
            </Button>
          </Box>

        </Box>
      </Box>

    </Box>
  )
}

export default Profile