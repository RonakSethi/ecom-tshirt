import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { Box, Button, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Header from "components/Header";
import { useDeleteRecord } from "hooks";
import { useDispatch } from "react-redux";
import { useGetCaptionTagQuery } from "services";
import { setShowMasterModal } from "store/slices/utilSlice";
import { tokens } from "theme";
import dataGridStyles from "../../../styles/dataGridStyles";

const List = () => {
  // hooks
  const dispatch = useDispatch();

  // queries
  const {
    data: captionTagData = [],
    isLoading,
    refetch,
  } = useGetCaptionTagQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  // hooks
  const { deleteRecord } = useDeleteRecord();

  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const colors = tokens(theme.palette.mode);
  const columns = [
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "sortOrder",
      headerName: "Sort Order",
      flex: 1,
    },
    {
      headerName: "Action",
      flex: 1,
      renderCell: (params) => (
        <Box display="flex" justifyContent="center" gap={2}>
          <span>
            <EditIcon />
          </span>
          <span
            className="cursor-pointer"
            onClick={() => {
              const payload = {
                modelName: "CaptionTag",
                status: true,
                modelId: params.row._id,
              };
              deleteRecord(payload, refetch);
            }}
          >
            <DeleteIcon />
          </span>
        </Box>
      ),
    },
  ];

  const handleOpen = () => {
    dispatch(setShowMasterModal(true));
  };

  return (
    <Box m="20px">
      <Box
        sx={{
          ...styles.headingsContainer,
          ...styles.dFlex,
          ...styles.alignItemsStart,
          ...styles.justifyContentBetween,
        }}
      >
        <Box sx={{ ...styles.mainHeadings }}>
          <Header title="CAPTION TAGS" subtitle="Managing the Caption Tags" />
        </Box>
        <Box>
          <Button sx={styles.buttonMd} onClick={handleOpen}>
            <AddOutlinedIcon sx={{ mr: "10px" }} />
            Add Caption Tag
          </Button>
        </Box>
      </Box>
      <Box
        m="0px 0 0 0"
        height="75vh"
        sx={{
          ...styles.listContainer,
        }}
      >
        <DataGrid
          rows={captionTagData}
          columns={columns}
          getRowId={(row) => row._id}
          loading={isLoading}
        />
      </Box>
    </Box>
  );
};

export default List;
