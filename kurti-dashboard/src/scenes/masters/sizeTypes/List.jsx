import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { Box, Button, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Header from "components/Header";
import { useDeleteRecord } from "hooks";
import { useDispatch } from "react-redux";
import { useGetSizeTypesQuery } from "services";
import { setShowMasterModal } from "store/slices/utilSlice";
import { tokens } from "theme";
import dataGridStyles from "../../../styles/dataGridStyles";

const List = () => {
  // hooks
  const dispatch = useDispatch();

  // queries
  const {
    data: sizeTypesData = [],
    isLoading,
    refetch,
  } = useGetSizeTypesQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  // hooks
  const { deleteRecord } = useDeleteRecord();

  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const colors = tokens(theme.palette.mode);
  const columns = [
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "values",
      headerName: "Values",
      flex: 1,
      renderCell: (params) => {
        const values = params.row.values;
        return (
          <Box display="flex" justifyContent="center">
            {values.map((value) => (
              <Box
                key={value}
                sx={{
                  backgroundColor: colors.blueAccent[600],
                  color: colors.whiteAccent[100],
                  fontSize: "12px",
                  fontWeight: "normal",
                  padding: "5px 10px",
                  margin: "0 5px",
                  borderRadius: "5px",
                }}
              >
                {value}
              </Box>
            ))}
          </Box>
        );
      },
    },
    {
      headerName: "Action",
      flex: 1,
      renderCell: (params) => (
        <Box display="flex" justifyContent="center" gap={2}>
          <span>
            <EditIcon />
          </span>
          <span
            className="cursor-pointer"
            onClick={() => {
              const payload = {
                modelName: "SizeType",
                status: true,
                modelId: params.row._id,
              };
              deleteRecord(payload, refetch);
            }}
          >
            <DeleteIcon />
          </span>
        </Box>
      ),
    },
  ];

  const handleOpen = () => {
    dispatch(setShowMasterModal(true));
  };

  return (
    <Box m="20px">
      <Box
        sx={{
          ...styles.headingsContainer,
          ...styles.dFlex,
          ...styles.alignItemsStart,
          ...styles.justifyContentBetween,
        }}
      >
        <Box sx={{ ...styles.mainHeadings }}>
          <Header title="SIZE TYPES" subtitle="Managing the Size Types" />
        </Box>
        <Box>
          <Button sx={styles.buttonMd} onClick={handleOpen}>
            <AddOutlinedIcon sx={{ mr: "10px" }} />
            Add Size Type
          </Button>
        </Box>
      </Box>
      <Box
        m="0px 0 0 0"
        height="75vh"
        sx={{
          ...styles.listContainer,
        }}
      >
        <DataGrid
          rows={sizeTypesData}
          columns={columns}
          getRowId={(row) => row._id}
          loading={isLoading}
        />
      </Box>
    </Box>
  );
};

export default List;
