import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { Box, Button, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Header from "components/Header";
import { useDeleteRecord } from "hooks";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useGetColorsQuery } from "services";
import { setShowMasterModal } from "store/slices/utilSlice";
import { tokens } from "theme";
import dataGridStyles from "../../../styles/dataGridStyles";

const List = () => {
  // hooks
  const dispatch = useDispatch();
  const { deleteRecord } = useDeleteRecord();

  // queries
  const {
    data: colorsData = [],
    isLoading,
    refetch,
  } = useGetColorsQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  // states
  const [open, setOpen] = useState(false);

  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const colors = tokens(theme.palette.mode);
  const columns = [
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "sortOrder",
      headerName: "Sorted Order",
      flex: 1,
    },
    {
      field: "hexCode",
      headerName: "Color",
      flex: 1,
      renderCell: (params) => {
        const hexCode = params.row.hexCode;
        return (
          <>
            <Box
              sx={{
                backgroundColor: hexCode,
                fontSize: "12px",
                fontWeight: "bold",
                padding: "15px",
                borderRadius: "5px",
                margin: "0 5px",
              }}
            />
            {hexCode}
          </>
        );
      },
    },
    {
      headerName: "Action",
      flex: 1,
      renderCell: (params) => (
        <Box display="flex" justifyContent="center" gap={2}>
          <span>
            <EditIcon />
          </span>
          <span
            className="cursor-pointer"
            onClick={() => {
              const payload = {
                modelName: "Color",
                status: true,
                modelId: params.row._id,
              };
              deleteRecord(payload, refetch);
            }}
          >
            <DeleteIcon />
          </span>
        </Box>
      ),
    },
  ];

  const handleOpen = () => {
    dispatch(setShowMasterModal(true));
  };

  return (
    <>
      <Box m="20px">
        <Box
          sx={{
            ...styles.headingsContainer,
            ...styles.dFlex,
            ...styles.alignItemsStart,
            ...styles.justifyContentBetween,
          }}
        >
          <Box sx={{ ...styles.mainHeadings }}>
            <Header title="COLORS" subtitle="Managing the Colors" />
          </Box>
          <Box>
            <Button sx={styles.buttonMd} onClick={handleOpen}>
              <AddOutlinedIcon sx={{ mr: "10px" }} />
              Add Color
            </Button>
          </Box>
        </Box>
        <Box
          m="0px 0 0 0"
          height="75vh"
          sx={{
            ...styles.listContainer,
          }}
        >
          <DataGrid
            rows={colorsData}
            columns={columns}
            getRowId={(row) => row._id}
            loading={isLoading}
          />
        </Box>
      </Box>
    </>
  );
};

export default List;
