import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import { Box, Button, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Header from "components/Header";
import { useDispatch } from "react-redux";
import { useGetPaymentModesQuery } from "services";
import { setShowMasterModal } from "store/slices/utilSlice";
import { tokens } from "theme";
import dataGridStyles from "../../../styles/dataGridStyles";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { useDeleteRecord } from "hooks";

const List = () => {
  // hooks
  const dispatch = useDispatch();
  const { deleteRecord } = useDeleteRecord();

  // queries
  const {
    data: paymentModesData = [],
    isLoading,
    refetch,
  } = useGetPaymentModesQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const colors = tokens(theme.palette.mode);
  const columns = [
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "sortOrder",
      headerName: "Sort Order",
      flex: 1,
    },
    {
      headerName: "Action",
      flex: 1,
      renderCell: (params) => (
        <Box display="flex" justifyContent="center" gap={2}>
          <span>
            <EditIcon />
          </span>
          <span
            className="cursor-pointer"
            onClick={() => {
              const payload = {
                modelName: "PaymentMode",
                status: true,
                modelId: params.row._id,
              };
              deleteRecord(payload, refetch);
            }}
          >
            <DeleteIcon />
          </span>
        </Box>
      ),
    },
  ];

  const handleOpen = () => {
    dispatch(setShowMasterModal(true));
  };

  return (
    <Box m="20px">
      <Box
        sx={{
          ...styles.headingsContainer,
          ...styles.dFlex,
          ...styles.alignItemsStart,
          ...styles.justifyContentBetween,
        }}
      >
        <Box sx={{ ...styles.mainHeadings }}>
          <Header title="PAYMENT MODES" subtitle="Managing the Payment Modes" />
        </Box>
        <Box>
          <Button sx={styles.buttonMd} onClick={handleOpen}>
            <AddOutlinedIcon sx={{ mr: "10px" }} />
            Add Payment Mode
          </Button>
        </Box>
      </Box>
      <Box
        m="0px 0 0 0"
        height="75vh"
        sx={{
          ...styles.listContainer,
        }}
      >
        <DataGrid
          rows={paymentModesData}
          columns={columns}
          getRowId={(row) => row._id}
          loading={isLoading}
        />
      </Box>
    </Box>
  );
};

export default List;
