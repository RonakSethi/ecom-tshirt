import { Box, Button, Typography, useTheme } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "components/Header";
import parse from "html-react-parser";
import { useNavigate, useParams } from "react-router-dom";
import { useGetCategoriesQuery } from "services";
import dataGridStyles from "../../styles/dataGridStyles";

const View = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const { id } = useParams();

  const { data: categoryData } = useGetCategoriesQuery(id, {
    skip: !id,
  });

  const navigate = useNavigate();

  return (
    <Box m="20px">
      <Box sx={styles.mainHeadings}>
        <Header title="CATEGORY DETAILS" />

        <Box display="flex" justifyContent="end" mt="20px">
          <Button
            onClick={() => navigate("/categories")}
            color="secondary"
            variant="contained"
          >
            Back to Categories
          </Button>
        </Box>
      </Box>
      <Box sx={styles.formContainer}>
        <Box
          display="grid"
          gap="30px"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
          sx={{ "& > div": { gridColumn: isNonMobile ? undefined : "span 4" } }}
        >
          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Slug:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {categoryData?.slug}
          </Typography>

          <Typography variant="h6" sx={{ gridColumn: "span 1" }}>
            Name:
          </Typography>
          <Typography sx={{ gridColumn: "span 1" }}>
            {categoryData?.name}
          </Typography>

          <Box sx={{ gridColumn: "span 4" }}>
            <Typography variant="h6">Description:</Typography>
            <Typography>{parse(categoryData?.description ?? "")}</Typography>
          </Box>

          <Box sx={{ gridColumn: "span 4", marginTop: "30px" }}>
            <Typography variant="h6">Main Image:</Typography>
            <img src={categoryData?.image} alt="Main" />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default View;
