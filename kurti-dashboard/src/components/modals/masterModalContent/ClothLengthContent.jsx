import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material";
import {
  captionTagInitialValues,
  captionTagValidationSchema,
} from "constants/schemas/masters";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { useAddClothLengthMutation } from "services";
import { setShowMasterModal } from "store/slices/utilSlice";

import dataGridStyles from "../../../styles/dataGridStyles";

const ClothLengthContent = () => {
  const dispatch = useDispatch();
  // mutations
  const [clothLengthMutation] = useAddClothLengthMutation();

  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);
  const formik = useFormik({
    initialValues: captionTagInitialValues,
    validationSchema: captionTagValidationSchema,
    onSubmit: (values) => {
      clothLengthMutation(values);
      dispatch(setShowMasterModal(false));
    },
  });
  return (
    <Box sx={{ ...styles.formContainer, ...styles.modalContainer, }}>
      <Typography variant="h6"  gutterBottom>
        Add New Cloth Length
      </Typography>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          label="Name"
          variant="outlined"
          fullWidth
          margin="normal"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.name}
          name="name"
          error={formik.touched.name && Boolean(formik.errors.name)}
        />
        <TextField
          type="number"
          label="Sort Order"
          variant="outlined"
          fullWidth
          margin="normal"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.sortOrder}
          name="sortOrder"
          error={formik.touched.sortOrder && Boolean(formik.errors.sortOrder)}
        />
        <Button type="submit" variant="contained" color="primary">
          Submit
        </Button>
      </form>
    </Box>
  );
};

export default ClothLengthContent;
