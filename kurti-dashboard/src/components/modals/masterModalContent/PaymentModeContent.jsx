import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import {
  paymentModeInitialValues,
  paymentModeValidationSchema,
} from "constants/schemas/masters";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { useAddPaymentModeMutation } from "services";
import { setShowMasterModal } from "store/slices/utilSlice";
import { useTheme } from "@mui/material";
import dataGridStyles from "../../../styles/dataGridStyles";

const PaymentModeContent = () => {
  const dispatch = useDispatch();
  // mutations
  const [paymentModeMutation] = useAddPaymentModeMutation();
  const theme = useTheme();
  const styles = dataGridStyles(theme.palette.mode);

  const formik = useFormik({
    initialValues: paymentModeInitialValues,
    validationSchema: paymentModeValidationSchema,
    onSubmit: (values) => {
      paymentModeMutation(values);
      dispatch(setShowMasterModal(false));
    },
  });

  return (
    <Box sx={{ ...styles.formContainer, ...styles.modalContainer, }}>
      <Typography variant="h6"  gutterBottom>
        Add New Payment Mode
      </Typography>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          label="Name"
          variant="outlined"
          fullWidth
          margin="normal"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.name}
          name="name"
          error={formik.touched.name && Boolean(formik.errors.name)}
          className="input"
        />
        <TextField
          type="number"
          label="Sort Order"
          variant="outlined"
          fullWidth
          margin="normal"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.sortOrder}
          name="sortOrder"
          error={formik.touched.sortOrder && Boolean(formik.errors.sortOrder)}
          className="input fieldset-1"
        />
        <Button type="submit" variant="contained" color="primary">
          Submit
        </Button>
      </form>
    </Box>
  );
};

export default PaymentModeContent;
