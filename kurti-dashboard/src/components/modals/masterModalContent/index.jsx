export { default as BrandsContent } from "./BrandsContent";
export { default as CaptionTagContent } from "./CaptionTagContent";
export { default as ClothLengthContent } from "./ClothLengthContent";
export { default as ColorContent } from "./ColorContent";
export { default as DiscountContent } from "./DiscountContent";
export { default as FabricContent } from "./FabricContent";
export { default as OccasionsContent } from "./OccasionsContent";
export { default as PatternsContent } from "./PatternsContent";
export { default as PaymentModeContent } from "./PaymentModeContent";
export { default as SectionContent } from "./SectionContent";
export { default as SizeTypeContent } from "./SizeTypeContent";

