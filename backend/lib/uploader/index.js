const fs = require('fs');
const AWS = require('aws-sdk');
const { logError, randomString } = require('../util');
const accessKeyId = process.env.AWS_S3_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_S3_SECRET_ACCESS_KEY;
const region = process.env.AWS_S3_REGION;
const bucket = process.env.AWS_S3_BUCKET;
const awsConfig = {
    accessKeyId,
    secretAccessKey,
    region,
    apiVersion: '2006-03-01',
    signatureVersion: 'v4',
    ACL: 'public-read',
};
process.env.NODE_ENV === 'production' && ['accessKeyId','secretAccessKey'].forEach(i => delete awsConfig[i]);
AWS.config.update(awsConfig);
const s3 = new AWS.S3({
    sslEnabled: process.env.AWS_S3_SECURE === 'true'
});
const S3_BASE = `https://${bucket}.s3.${region}.amazonaws.com/`;

const getSignedUrl = (location, extension) =>
    new Promise((resolve, reject) => {
        const key = `${location}${randomString()}.${extension}`;
        s3.getSignedUrl(
            'putObject',
            {
                Bucket: process.env.AWS_S3_BUCKET,
                Key: key,
                ACL: 'public-read',
            },
            (err, data) => {
                if (err) return reject(err);
                resolve({
                    url: data,
                    preview: `${S3_BASE}${key}`,
                });
            }
        );
    });

const upload = (files, location, extension = 'jpg') => {
    const urlsArr = [];
    const onErr = err => {
        logError("Error in uploading file: ", err);
        return null;
    };
    files = !Array.isArray(files) ? [files] : files;
    for(let i = 0; i < files.length; i++){
        const file  = files[i];
        const fileUrl = `${location}/${randomString()}.${extension}`;

        fs.readFile(file.tempFilePath, (err, buffer) => {
            err && onErr(err);
            s3.putObject({
                Bucket: bucket,
                Key: fileUrl,
                Body: buffer,
                ACL: 'public-read'
            },  uploadErr => {
                uploadErr && onErr(uploadErr);
            });
        });
        urlsArr.push(fileUrl);
    }
    return urlsArr && urlsArr.length === 1 ? urlsArr[0] : urlsArr;
};

const deleteObj = Key => new Promise(resolve => {
    s3.deleteObject({
        Bucket: bucket,
        Key
    }, error => {
        if (error) {
            logError("Error in uploading file: ", error);
            resolve(false);
        }
        resolve(true);
    });
});

module.exports = {
    getSignedUrl,
    upload,
    deleteObj
};