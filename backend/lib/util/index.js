const mongoose = require('mongoose');
const moment = require('moment');
const fcmNode = require('fcm-node');
const fcm = new fcmNode(process.env.FCM_KEY);


const randomString = (length = 30, charSet = 'ABC5DEfF78G7I5JKL8MNO7PQR8ST5UVnaSdWXYZa5bjcFh6ijk123456789') => {
    let randomString = '';
    for (let i = 0; i < length; i++) {
        let randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
};

const escapeRegex = text => text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');

// eslint-disable-next-line no-console
const logError = console.error;

const consoleInDevEnv = process.env.NODE_ENV === 'development' && console.log;

/**
 * @param {string} objectId
 * @return {boolean}
 */
const isValidObjectId = objectId => {
    if (mongoose.Types.ObjectId.isValid(objectId)) {
        const id = new mongoose.Types.ObjectId(objectId);
        return id.toString() === objectId;
    }
    return false;
};

const utcDate = (date = new Date()) => {
    date = new Date(date);
    return new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0));
};

const utcDateTime = (date = new Date()) => {
    date = new Date(date);
    return new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()));
};


const generateResetToken = (length = 4) => {
    return Math.floor(Math.pow(10, length - 1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1));
};

const showDate = (date, format = 'MMM DD YYYY hh:mm A', toLocale = true) => {
    date = toLocale ? new Date(date).toLocaleString() : date;
    return utcDateTime(date).toString() !== 'Invalid Date' ? moment.utc(date).format(format) : 'N/A'
};

const showTime = seconds => new Date(seconds * 1000).toISOString().substr(11, 8);

const fromNow = date => moment(date).fromNow();

const sendFCMPush = (tokens, title, body, data = {}, priority = 'high', notificationOptions = {
    click_action: "FCM_PLUGIN_ACTIVITY",
    icon: "ic_stat_icon",
    sound: "default",
    vibrate: true,
}) => {
    tokens = !Array.isArray(tokens) ? [tokens] : tokens;
    const fcmMessage = {
        registration_ids: tokens,
        priority,
        notification: {
            title,
            body,
            ...notificationOptions
        },
        data: {
            PATH: {
                data
            }
        }
    };
    fcm.send(fcmMessage, err => {
        if (err) logError("FCM ERROR: ", err);
    });

 
};

module.exports = {
    escapeRegex,
    logError,
    consoleInDevEnv,
    isValidObjectId,
    utcDate,
    utcDateTime,
    randomString,
    generateResetToken,
    showDate,
    showTime,
    fromNow,
    sendFCMPush
};
