const locale = {
    GENERAL_ERROR: 'An error occurred.',
    WELCOME: 'Welcome !',
    DROP_ZONE_DROP_MSG: 'Drop file here or click to upload.',
    DROP_ZONE_DROP_UPLOAD_LIMIT: 'You can upload image of maximum,',
    DROP_FILE: 'Drop file here or click to upload.',
    UPLOAD_FILE_SIZE: 'You can upload image of maximum',
    IMAGE_DELETE_FAILED: 'Image Deletion Failed.',
    IMAGE_DELETE_SUCCESS: 'Image Deleted Successfully.',
    VIDEO_DELETE_FAILED: 'Video Deletion Failed.',
    VIDEO_DELETE_SUCCESS: 'Video Deleted Successfully.',
    UNAUTHORIZED: 'Unauthorized access.',
    USER_NOT_FOUND: 'This user isn\'t registered with us.',
    EMAIL_ALREADY_VERIFIED: 'Your email is already verified.',
    USER_VERIFIED: 'User verified successfully.',
    USER_ALREADY_FOUND: 'Account already registered with your phone or email.',
    PHONE_ALREADY_FOUND: 'Account already registered with this phone number.',
    EMAIL_ALREADY_FOUND: 'Account already registered with this email address.',
    USER_STATUS_UPDATED: 'User status updated successfully.',
    USER_NOT_EXIST: 'User not exist.',
    USER_DELETED: 'User deleted successfully.',
    USER_UPDATED: 'User updated successfully.',
    INVALID_CREDENTIALS: 'Invalid Username or Password.',
    INVALID_USER_CREDENTIALS: 'Invalid phone or Password.',
    PHONE_NOT_VERIFIED: 'Phone not verified.',
    ACCOUNT_NOT_VERIFIED: 'Your account is not verified.',
    YOUR_ACCOUNT_SUSPENDED: 'Your account is suspended.',
    YOUR_ACCOUNT_NOT_VERIFIED: 'Your account is not verified, please contact to admin.',
    EMAIL_CHANGED: 'Email changed successfully.',
    PHONE_CHANGED: 'Phone changed successfully.',
    NOTIFICATION_TURNED_ON: 'Notification turned on successfully.',
    NOTIFICATION_TURNED_OFF: 'Notification turned off successfully.',
    PASSWORD_CHANGED: 'Password changed successfully.',
    PASSWORD_MATCH_FAILURE: 'Entered password doesn\'t match the old password.',
    PROFILE_UPDATED: 'Profile updated successfully.',
    OTP_SENT: 'OTP sent successfully.',
    OTP_VERIFIED: 'OTP verified successfully.',
    INVALID_OTP: 'Invalid OTP.',
    LOGIN_SUCCESS: 'Logged-In successfully.',
    LOGOUT_SUCCESS: 'Logged-Out successfully.',
    passwordPattern: 'at least 8 characters with at least 1 alphabet and 1 digit.',
    adminPasswordPattern: 'Password must contain at least 8 characters with at least 1 alphabet (upper & lowe cased), 1 digit. & 1 special character.',
    countryCodePattern: 'plus sign followed by digits.',
    otpPattern: 'a valid otp.',
    INVALID_RESET_PASS_REQUEST: 'Invalid reset password request.',
    FORGOT_PASSWORD_MAIL_SUCCESS: 'Forgot password email sent successfully. Please check and change password.',
    EMAIL_RESET_PASSWORD: 'Reset password email.',
    WEB_ACCOUNT_CREATED: 'Account created successfully.',
    ACCOUNT_ALREADY_VERIFIED: 'Account already verified.',
    USERNAME_MATCHED: 'User name already exists.',
    EMAIL_MATCHED: 'Email already exists.',
    USERNAME_EMAIL_MATCHED: 'User name or email already exists.',
    OTP_SEND: 'Otp send successfully.',
    FORGOT_EMAIL_SEND: 'Forgot email send. Please check your email-id and reset your password.',
    CONTACT_US_SEND: 'Contact mail send successfully.',
    EMAIL_CONTACT_US: 'Contact us mail',
    FORGOT_PASSWORD: 'Reset Password',
    EMAIL_NOT_MATCHED: 'Email address not registered with us.',
    FILE_MAX_SIZE_ERR: 'File size must be less than or equal to {0} mb.',
    SETTINGS_UPDATE_SUCCESS: 'Settings updated successfully.',
    PAYMENT_SUCCESS_TITLE: 'Success!',
    PAYMENT_ERROR_TITLE: 'Error!',
    ALREADY_PAID: 'Already paid.',
    REDIRECT_TO_PAYMENTS: 'Redirecting to payment gateway.',
    INVALID_CREDENTIALS_LIMIT: 'Invalid credentials{0}',
    LOGIN_DISABLED: 'Login disabled for {0}.',
    KEY_MINUTES: 'minutes &',
    KEY_SECONDS: 'seconds',
    KEY_LOGIN_DISABLED: ', login disabled.',
    KEY_YOU_HAVE_ONLY: ', you have only',
    KEY_CHANCE_LEFT: 'chance left.',
    CATEGORY_ADDED: 'Category added successfully.',
    SUBCATEGORY_ADDED: 'Subcategory added successfully.',
    CATEGORY_NOT_EXIST: 'Category not exists.',
    CATEGORY_STATUS_UPDATED: 'Category status updated successfully.',
    CATEGORY_UPDATED: 'Category updated successfully.',
    CATEGORY_DELETED: 'Category deleted successfully.',
    EMAIL_ALREADY_EXISTS: 'Email already exists.',
    CONTACT_NO_ALREADY_EXISTS: 'Contact number already exists.',
    DESCRIPTION_REQUIRED: 'Description is required.',
    CONTACT_REQUEST_SUBMITTED: 'Contact request submitted successfully.',
    CONTACT_US_DELETED:'Contact us request deleted successfully.',
    CONTACT_US_NOT_EXISTS:'Contact us request not exists.',
    CONTACT_US_STATUS_UPDATED:"Contact us request status updated successfully.",
    PAGE_NOT_EXISTS: 'Page not exists.',
    PAGE_STATUS_UPDATED: 'Page status updated successfully.',
    PAGE_UPDATED: 'Page updated successfully.',

    STATIC_PAGE_EDIT: {
        title: {
            required: 'Page title is required.',
            minlength: 'Page title is required to be minimum 3 chars.',
            maxlength: 'Page title is required to be maximum 30 chars.',
        },
        titleRU: {
            required: 'Page title is required.',
            minlength: 'Page title is required to be minimum 3 chars.',
            maxlength: 'Page title is required to be maximum 30 chars.',
        },
        titleKK: {
            required: 'Page title is required.',
            minlength: 'Page title is required to be minimum 3 chars.',
            maxlength: 'Page title is required to be maximum 30 chars.',
        },
    },
    CATEGORY_ADD_FORM: {
        categoryName: {
            required: 'Name is required.',
            minlength: 'Name is required to be minimum 3 chars.',
            maxlength: 'Name is required to be maximum 30 chars.',
        },
    },
    CATEGORY_EDIT_FORM: {
        categoryName: {
            required: 'Category Name is required.',
            minlength: 'Email address is required to be minimum 2 chars.',
            maxlength: 'Email address is required to be maximum 30 chars.',
        },
    },
    ADMIN_LOGIN_FORM: {
        email: {
            required: 'Email address is required.',
            minlength: 'Email address is required to be minimum 3 chars.',
            maxlength: 'Email address is required to be maximum 80 chars.',
            pattern: 'Email address is required to be a valid email.',
        },
        password: {
            required: 'Password is required.',
        },
    },
    ADMIN_FORGOT_PASSWORD_FORM: {
        email: {
            required: 'Email address is required.',
            minlength: 'Email address is required to be minimum 3 chars.',
            maxlength: 'Email address is required to be maximum 80 chars.',
            pattern: 'Email address is required to be a valid email.',
        },
    },
    ADMIN_RESET_PASSWORD_FORM: {
        newPassword: {
            required: 'Password is required.',
            pattern: 'Password must contain at least 8 characters with at least 1 alphabet (upper & lowe cased), 1 digit. & 1 special character.',
        },
        otp: {
            required: 'OTP is required.',
            pattern: 'OTP is required to be a valid otp.'
        },
    },
    PROFILE_FORM: {
        firstName: {
            required: 'First name is required.',
            minlength: 'First name is required to be minimum 3 chars.',
            maxlength: 'First name is required to be minimum 30 chars.',
        },
        lastName: {
            required: 'Last name is required.',
            minlength: 'Last name is required to be minimum 3 chars.',
            maxlength: 'Last name is required to be minimum 30 chars.',
        },
        email: {
            required: 'Email address is required.',
            minlength: 'Email address is required to be minimum 3 chars.',
            maxlength: 'Email address is required to be maximum 80 chars.',
            pattern: 'Please enter a valid email address.',
            remote: 'This email address is already Exists.',
        },
        countryCode: {
            required: 'Country code is required.',
            pattern: 'Country code is required to be a valid code.',
        },
        contactNumber: {
            required: 'Contact number is required.',
            pattern: 'Contact number is required to be a valid number.',
        },
    },
    CHANGE_PASSWORD: {
        currentPassword: {
            required: 'Current password is required.',
        },
        newPassword: {
            required: 'New password is required.',
            pattern: 'Password must contain at least 8 characters with at least 1 alphabet (upper & lowe cased), 1 digit. & 1 special character.',
        },
        confirmPassword: {
            required: 'Confirm password is required.',
            equalTo: 'Confirm password must match new password.',
        },
    },
    ADMIN_SETTINGS_FORM: {
        androidAppVersion: {
            required: 'Android app version is required.',
            pattern: 'Only semantic versions is allowed.',
        },
        iosAppVersion: {
            required: 'iOS app version is required.',
            pattern: 'Only semantic versions is allowed.',
        },
        androidForceUpdate: {
            required: 'Android force update is required.',
        },
        iosForceUpdate: {
            required: 'iOS force update is required.',
        },
        maintenance: {
            required: 'Maintenance is required.',
        },
    }
};

const validationKeys = {
    firstName: 'First name',
    lastName: 'Last name',
    fullName: 'Full name',
    name: 'Name',
    userName: 'User name',
    email: 'Email Address',
    countryCode: 'Country code',
    contactNumber: 'Contact number',
    password: 'Password',
    currentPassword: 'Current Password',
    newPassword: 'New Password',
    confirmPassword: 'Confirm Password',
    emailPattern: 'a valid email address',
    contactNumberPattern: 'a valid number',
    phonePattern: 'a valid number',
    passwordPattern: 'at least 8 characters with at least 1 alphabet and 1 digit',
    adminPasswordPattern: 'Password must contain at least 8 characters with at least 1 alphabet (upper & lowe cased), 1 digit. & 1 special character.',
    countryCodePattern: 'plus sign followed by digits.',
    otpPattern: 'a valid otp.',
    timeIn24Hours: 'a valid time in 24 hours format',
    language: 'Language',
    title: 'Title',
    description: 'Description',
    dateFrom: 'From date',
    dateTo: 'To date',
    status: 'Status',
    id: 'ID',
    path: 'Path',
    parent: 'Parent',
    main: 'valid object id or "main"',
    category: 'Category',
    originalPrice: 'Original price',
    price: 'Price',
    address: 'Address',
    avatar: 'Avatar',
    deviceToken: 'Device token',
    landmark: 'Landmark',
    street: 'Street',
    latitude: 'Latitude',
    longitude: 'Longitude',
    page: 'page',
    perPage: 'Per page',
    radius: 'Radius',
};

const key = keyName => validationKeys[keyName.replace(/\.[\d]+/, '')] || keyName;

/**
 * @see https://github.com/hapijs/joi/blob/master/lib/language.js
 */
const validationMessages = {
    any: {
        required: ({ path }) => `${key(path.join('.'))} is required.`,
        unknown: ({ path }) => `${key(path.join('.'))} is not allowed.`,
        invalid: ({ path }) => `${key(path.join('.'))} contains an invalid value.`,
        empty: ({ path }) => `${key(path.join('.'))} is required.`,
        allowOnly: ({ context, path }) => `${key(path.join('.'))} must be one of ${context.valids.map(item => key(item)).join(', ')}`,
    },

    string: {
        regex: {
            name: ({ context, path }) => `${key(path.join('.'))} must contain ${key(context.name)}`,
        },
        min: ({ context, path }) => `${key(path.join('.'))} must be at least ${context.limit} characters in length.`,
        max: ({ context, path }) => `${key(path.join('.'))} must be under ${context.limit} characters in length.`,
        hex: ({ path }) => `${key(path.join('.'))} must only contain hexadecimal characters.`,
        length: ({ path }) => `${key(path.join('.'))} length must be 4 characters long.`,
    },

    number: {
        base: ({ path }) => `${key(path.join('.'))} must be a number`,
        min: ({ context, path }) => `${key(path.join('.'))} must be larger than or equal to ${context.limit}.`,
        max: ({ context, path }) => `${key(path.join('.'))} must be less than or equal to ${context.limit}.`,
        integer: ({ path }) => `${key(path.join('.'))} must be a integer number.`,
    },

    objectId: {
        valid: ({ path }) => `${key(path.join('.'))} needs to be a valid Object ID`,
    },

    object: {
        base: ({ path }) => `${key(path.join('.'))} must be an object`,
        xor: ({ context }) => `only one of ${context.peers.map(peer => key(peer)).join(', ')} is allowed.`,
        with: ({ context }) => `${key(context.peer)} is required with ${key(context.main)}.`,
        without: ({ context }) => `${key(context.peer)} needs to be removed with ${key(context.main)}.`,
        and: ({ context }) =>
            `${context.missing.map(peer => key(peer)).join(', ')} required with ${context.present
                .map(peer => key(peer))
                .join(', ')}.`,
        missing: ({ context }) => `one of ${context.peers.map(peer => key(peer).toLowerCase()).join(', ')} is required.`,
    },

    array: {
        min: ({ path, context }) => `${key(path.join('.'))} must contain at least ${context.limit} items.`,
        max: ({ path, context }) => `${key(path.join('.'))} must contain at most ${context.limit} items.`,
    },

    custom: {
        sameAs: (key1, key2) => `${key(key1)} must match the ${key(key2)} field.`,
    },

    date: {
        valid: key1 => `${key(key1)} needs to be a valid date.`,
        min: (key1, date) => `${key(key1)} must be larger than or equal to ${date}.`,
        max: (key1, date) => `${key(key1)} must be less than or equal to ${date}.`,
        invalid: key1 => `${key(key1)} is required to be a valid date.`,
    },
};

locale.validationKeys = validationKeys;
locale.validation = validationMessages;

module.exports = Object.freeze(locale);