module.exports = {
    AdminRole: require('./AdminRole.enum'),
    Platform: require('./Platform.enum'),
    UploadConfig: require('./UploadConfig.enum'),
    OtpType: require('./OtpType.enum'),
};
