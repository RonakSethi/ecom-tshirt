const OtpType = Object.freeze({
    SIGN_UP: 'Sign up',
    FORGOT_PASSWORD: 'Forgot password',
    CHANGE_PHONE: 'Change phone',
});

module.exports = OtpType;
