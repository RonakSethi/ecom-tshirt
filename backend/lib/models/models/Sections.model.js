const mongoose = require('mongoose');

const SectionsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Sections = mongoose.model('Sections', SectionsSchema);

module.exports = Sections;
