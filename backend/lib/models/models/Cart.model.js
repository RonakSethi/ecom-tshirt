const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const CartSchema = new Schema(
    {
        userId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        products: [
            {
                productId: {
                    type: Schema.Types.ObjectId,
                    required: true,
                    ref: 'Product',
                },
                quantity: {
                    type: Number,
                    required: true,
                    min: 1,
                },
                selectedSize: {
                    type: String,
                    required: false,
                },
                selectedColor: {
                    name: String,
                    hexCode: String,
                },
                price: {
                    type: Number,
                    required: true,
                },
                total: {
                    type: Number,
                    required: true,
                },
                gTotal: {
                    type: Number,
                    required: true,
                },
                discount: {
                    type: Number,
                    required: true,
                },
            },
        ],
        totalPrice: {
            type: Number,
            required: true,
            default: 0,
        },
        gTotalPrice: {
            type: Number,
            required: true,
            default: 0,
        },
        totalDiscount: {
            type: Number,
            required: true,
            default: 0,
        },
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated',
        },
        id: false,
        toJSON: {
            getters: true,
        },
        toObject: {
            getters: true,
        },
    }
);

const Cart = mongoose.model('Cart', CartSchema);

module.exports = Cart;
