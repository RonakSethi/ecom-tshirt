const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
const { OtpType } = require('../enums');

const OtpSchema = new Schema(
    {
        type: {
            type: String,
            required: true,
            enum: [...Object.keys(OtpType)]
        },
        countryCode: {
            type: String,
            default: '+1'
        },
        phone: {
            type: String,
            trim: true,
            required: true,
        },
        formattedPhone: {
            type: String,
            trim: true,
        },
        token: {
            type: String,
            trim: true,
            required: true,
        },
        isVerified: {
            type: Boolean,
            default: false,
        },
        validTill: {
            type: Date,
        }
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated',
        },
        id: false,
        toJSON: {
            getters: true,
        },
        toObject: {
            getters: true,
        },
    }
);

OtpSchema.pre('save', async function (next) {
    const otp = this;
    if(otp.isModified('countryCode') || otp.isModified('phone')) {
        try {
            otp.formattedPhone = `${otp.countryCode}${otp.phone}`;
            next();
        }
        catch (e) {
            next(e);
        }
    } else {
        return next();
    }
});

module.exports = mongoose.model('Otp', OtpSchema);
