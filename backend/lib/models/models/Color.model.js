const mongoose = require('mongoose');

const ColorSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    hexCode: {
        type: String
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Color = mongoose.model('Color', ColorSchema);

module.exports = Color;
