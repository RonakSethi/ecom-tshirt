const mongoose = require('mongoose');

const FabricSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    image: {
        type: String,
        default: ''
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Fabric = mongoose.model('Fabric', FabricSchema);

module.exports = Fabric;
