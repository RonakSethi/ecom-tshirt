const mongoose = require('mongoose');

const CaptionTagSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

// Create and export the SizeType model
const CaptionTag = mongoose.model('CaptionTag', CaptionTagSchema);

module.exports = CaptionTag;
