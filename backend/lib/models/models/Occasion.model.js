const mongoose = require('mongoose');

const OccasionSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    image: {
        type: String,
        default: ''
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Occasion = mongoose.model('Occasion', OccasionSchema);

module.exports = Occasion;
