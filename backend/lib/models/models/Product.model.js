const mongoose = require('mongoose'),
Schema = mongoose.Schema;

const ProductSchema = new Schema(
    {
        sku: {
            type: String,
            trim: true,
            required: true,
        },
        name: {
            type: String,
            trim: true,
            required: true,
        },
        categoryId: {
            type: Schema.Types.ObjectId,
            default: null,
            ref: 'Category',
        },
        description: {
            type: String,
            default: '',
        },
        additionalInformation: {
            type: String,
            default: '',
        },
        type: {
            type: String,
            enum: ['all', 'B2B', 'B2C'],
            default: 'B2C',
        },
        attributes: {
            brand: String,
            sections: [String],
            occasion: [String],
            fabric: [String],
            length: String,
            pattern: String,
            color:[{
                name:String,
                hexCode:String
            } ],
            tags: [String],
            discount:String
        },
        isSize: {
            type: Boolean,
            default: false,
        },
        sizeType: [String],
        seo: {
            keywords: [String],
            metaTitle: String,
            metaDescription: String,
        },
        prices: {
            b2cPrice: {
                originalPrice: Number,
                sellingPrice: Number,
                discount: Number,
            },
            b2bPrice: {
                originalPrice: Number,
                sellingPrice: Number,
                discount: Number,
            },
        },
        stock: [{
            size: String,
            unitPrice: Number,
            sellingPrice: Number,
            minqty: Number,
            productType: String,
        }],
        mainImage: {
            type: String,
            default: '',
        },
        images: [String],
        reviews: [{
            userId: Schema.Types.ObjectId,
            rating: Number,
            comment: String,
            productId: Schema.Types.ObjectId,
            createdAt: { type: Date, default: Date.now },
        }],
        avgrating: Number,
        shopId: {
            type: String,
            required: true,
            ref: 'Shop',
        },
        shop: {
            type: Object,
            required: true,
        },
        sold_out: {
            type: Number,
            default: 0,
        },
        isSuspended: {
            type: Boolean,
            default: false,
        },
        isDeleted: {
            type: Boolean,
            default: false,
        },
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated',
        },
        id: false,
        toJSON: {
            getters: true,
        },
        toObject: {
            getters: true,
        },
    }
);

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
