const mongoose = require('mongoose');

const PaymentModeSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

// Create and export the SizeType model
const PaymentMode = mongoose.model('PaymentMode', PaymentModeSchema);

module.exports = PaymentMode;
