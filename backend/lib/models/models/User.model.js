const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt');

const UserSchema = new Schema(
    {
        userName: {
            type: String,
            trim: true,
        },
        fullName: {
            type: String,
            trim: true,
            required: true,
        },
        age: {
            type: Number,
            required: true,
        },
        countryCode: {
            type: String,
            trim: true,
            required: true,
        },
        phone: {
            type: String,
            trim: true,
            required: true,
        },
        formattedPhone: {
            type: String,
            trim: true,
        },
        password: {
            type: String,
            trim: true,
            required: true,
        },
        email: {
            type: String,
            trim: true,
            lowercase: true,
        },
        avatar: {
            type: String,
            trim: true,
        },
        deviceToken: {
            type: String,
            trim: true,
        },
        pushNotificationAllowed: {
            type: Boolean,
            default: true,
        },
        bio: {
            type: String,
            trim: true,
        },
        description: {
            type: String,
            trim: true,
        },
        isAccountComplete: {
            type: Boolean,
            default: false,
        },
        isSuspended: {
            type: Boolean,
            default: false,
        },
        isDeleted: {
            type: Boolean,
            default: false,
        },
        failedLoginAttempts: {
            type: Number,
            default: 0,
        },
        preventLoginTill: {
            type: Number,
            default: 0,
        },
        authTokenIssuedAt: Number
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated',
        },
        id: false,
        toJSON: {
            getters: true,
        },
        toObject: {
            getters: true,
        },
    },
);

UserSchema.pre('save', async function(next) {
    const user = this;
    if (user.isModified('password')) {
        try {
            const saltRounds = parseInt(process.env.BCRYPT_ITERATIONS, 10) || 10;
            user.password = await bcrypt.hash(user.password, saltRounds);
            next();
        }
        catch (e) {
            next(e);
        }
    }
    else if (user.isModified('countryCode') || user.isModified('phone')) {
        try {
            user.formattedPhone = `${user.countryCode}${user.phone}`;
            next();
        }
        catch (e) {
            next(e);
        }
    }
    else {
        return next();
    }
});

UserSchema.methods.comparePassword = async function(password) {
    try {
        return await bcrypt.compare(password, this.password);
    }
    catch (e) {
        return false;
    }
};

module.exports = mongoose.model('User', UserSchema);