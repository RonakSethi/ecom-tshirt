const mongoose = require('mongoose');

const DiscountSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    value: {
        type: Number,
        default: 0
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Discount = mongoose.model('Discount', DiscountSchema);

module.exports = Discount;
