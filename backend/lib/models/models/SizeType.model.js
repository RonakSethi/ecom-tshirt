const mongoose = require("mongoose");

const SizeTypeSchema  = new mongoose.Schema({
  name: { type: String, required: true }, // Name of the size type (e.g., "Clothing", "Shoe", "Accessory")
  values: [{ type: String, required: true }], // Possible values for the size type (e.g., ["Small", "Medium", "Large"])

  isSuspended: {
    type: Boolean,
    default: false,
},
isDeleted: {
    type: Boolean,
    default: false,
},
});


// Create and export the SizeType model
const SizeType = mongoose.model('SizeType', SizeTypeSchema);

module.exports = SizeType;
