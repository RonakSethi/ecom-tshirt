const mongoose = require('mongoose');

const BrandSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    image: {
        type: String,
        default: ''
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Brand = mongoose.model('Brand', BrandSchema);

module.exports = Brand;
