const mongoose = require('mongoose');

const PatternSchema = new mongoose.Schema({
    name: { 
      type: String,
       required: true 
      }, 
    image: {
        type: String,
        default: ''
    },
    sortOrder: {
        type: Number,
        default: 0,
    },
    isSuspended: {
        type: Boolean,
        default: false,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});

const Pattern = mongoose.model('Pattern', PatternSchema);

module.exports = Pattern;
