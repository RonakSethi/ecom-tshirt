const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const AdminSettings = new Schema(
    {
        androidAppVersion: {
            type: String,
            trim: true,
            required: true,
        },
        iosAppVersion: {
            type: String,
            trim: true,
            required: true,
        },
        androidForceUpdate: {
            type: Boolean,
            default: true,
        },
        iosForceUpdate: {
            type: Boolean,
            default: true,
        },
        maintenance: {
            type: Boolean,
            default: true,
        },
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated',
        },
        id: false,
        toJSON: {
            getters: true,
        },
        toObject: {
            getters: true,
        },
    });

module.exports = mongoose.model('AdminSettings', AdminSettings);