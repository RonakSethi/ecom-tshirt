const { Joi, common } = require('../../util/validations');

const optionalId = Joi.object().keys({
    id: Joi.objectId()
        .valid()
        .optional(),
});

const updateProfile = Joi.object().keys({
    type: Joi.string()
        .trim()
        .valid('PROFILE_UPDATE', 'ACCOUNT_COMPLETE')
        .required(),
    fullName: Joi.string().when('type', {
        is: 'PROFILE_UPDATE', then: Joi.string()
            .trim()
            .min(3)
            .max(30)
            .required(),
    }),
    age: Joi.string().when('type', {
        is: 'PROFILE_UPDATE', then: common.age,
    }),
    userName: Joi.string()
        .trim()
        .min(3)
        .max(30)
        .required(),
    bio: Joi.string()
        .trim()
        .min(3)
        .optional()
        .allow(''),
    description: Joi.string()
        .trim()
        .min(3)
        .optional()
        .allow(''),
    avatar: Joi.string()
        .trim()
        .max(500)
        .optional(),
});

const updatePassword = Joi.object().keys({
    currentPassword: Joi.string()
        .trim()
        .required(),
    newPassword: common.password,
});

const updateEmail = Joi.object().keys({
    currentPassword: Joi.string()
        .trim()
        .required(),
    email: common.email
});

const updatePhone = Joi.object().keys({
    currentPassword: Joi.string()
        .trim()
        .required(),
    countryCode: common.countryCode,
    phone: common.phone,
});

module.exports = {
    optionalId,
    updateProfile,
    updatePassword,
    updateEmail,
    updatePhone,
};