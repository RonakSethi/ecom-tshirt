const {
    models: { User, Otp },
} = require('../../../../lib/models');
const { utcDateTime } = require('../../../../lib/util');

class UserController {
    async list(req, res) {
        const { search, page = 1, limit = 10, orderby = 'desc' } = req.query;
        const finalResponse = {
            items: [],
            count: 0,
            page,
            limit,
        };
        // Convert page and limit to numbers
        const pageNumber = parseInt(page);
        const limitNumber = parseInt(limit);
        let sortOrder = orderby === 'desc' ? -1 : 1;

        // Calculate the number of items to skip
        let skip = (pageNumber - 1) * limitNumber;
        try {
            let qry = { isDeleted: false, fullName: { $ne: null } };

            if (search) {
                if (search) {
                    const searchValue = new RegExp(
                        search
                            .split(' ')
                            .filter(val => val)
                            .map(value => value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'))
                            .join('|'),
                        'i'
                    );

                    qry.$or = [
                        { name: searchValue },
                        { email: searchValue },
                        { countryCode: searchValue },
                        { mobile: searchValue },
                    ];
                }
            }

            let sortCond = { created: sortOrder };

            const dataCount = await User.count(qry);

            // Fetch all questions from the database and populate the subject and classId fields
            const users = await User.find(qry)
                .select('-password')
                .sort(sortCond)
                .skip(skip)
                .limit(limitNumber)
                .exec();

            // Check if there are any questions
            if (users.length === 0) {
                return res.warn(finalResponse, req.__('NO_USERS_FOUND'));
            }

            finalResponse.items = users;
            finalResponse.count = dataCount;

            // Respond with the list of questions, including populated subject and classId fields
            res.success(finalResponse);
        } catch (error) {
            // Handle errors
            console.log('Error fetching users:', error);
            res.warn(error);
        }
    }
    async profile(req, res) {
        let { user } = req;
        let { id } = req.query;

        id &&
            (user = await User.findOne({
                _id: id,
                isDeleted: false,
            }));

        if (!user) {
            return res.notFound('', req.__('USER_NOT_EXIST'));
        }

        if (user.isSuspended) {
            return res.notFound('', req.__('YOUR_ACCOUNT_SUSPENDED'));
        }

        user = JSON.parse(JSON.stringify(user));
        ['password', 'authTokenIssuedAt', 'failedLoginAttempts', 'preventLoginTill', 'social', '__v'].forEach(
            key => delete user[key]
        );

        return res.success(user);
    }

    async updateProfile(req, res) {
        const { type, fullName, age, userName, bio, description, avatar } = req.body;
        let { user } = req;

        const isUserNameExists = await User.findOne({
            _id: {
                $ne: user._id,
            },
            userName: new RegExp(`^${userName}$`, 'i'),
            isDeleted: false,
        });

        if (isUserNameExists) {
            return res.warn('', req.__('USERNAME_MATCHED'));
        }

        type === 'PROFILE_UPDATE' && fullName && (user.fullName = fullName);
        type === 'PROFILE_UPDATE' && age && (user.age = age);
        type === 'ACCOUNT_COMPLETE' && (user.isAccountComplete = true);
        user.userName = userName;
        user.bio = bio;
        user.description = description;
        avatar && (user.avatar = avatar);
        await user.save();

        user = JSON.parse(JSON.stringify(user));
        ['password', 'authTokenIssuedAt', 'failedLoginAttempts', 'preventLoginTill', 'social', '__v'].forEach(
            key => delete user[key]
        );

        return res.success(user, req.__('PROFILE_UPDATED'));
    }

    async updatePassword(req, res) {
        const { user } = req;
        const { currentPassword, newPassword } = req.body;

        const matched = await user.comparePassword(currentPassword);
        if (!matched) {
            return res.warn('', req.__('PASSWORD_MATCH_FAILURE'));
        }

        user.password = newPassword;
        await user.save();
        return res.success('', req.__('PASSWORD_CHANGED'));
    }

    async updateEmail(req, res) {
        const { user } = req;
        const { currentPassword, email } = req.body;

        const matched = await user.comparePassword(currentPassword);
        if (!matched) {
            return res.warn('', req.__('PASSWORD_MATCH_FAILURE'));
        }

        const isEmailExists = await User.findOne({
            _id: {
                $ne: user._id,
            },
            email,
            isDeleted: false,
        });

        if (isEmailExists) {
            return res.warn('', req.__('EMAIL_ALREADY_FOUND'));
        }

        user.email = email;
        await user.save();
        return res.success('', req.__('EMAIL_CHANGED'));
    }

    async updatePhone(req, res) {
        const { user } = req;
        const { currentPassword, countryCode, phone } = req.body;

        const matched = await user.comparePassword(currentPassword);
        if (!matched) {
            return res.warn('', req.__('PASSWORD_MATCH_FAILURE'));
        }

        const isPhoneExists = await User.findOne({
            _id: {
                $ne: user._id,
            },
            countryCode,
            phone,
            isDeleted: false,
        });

        if (isPhoneExists) {
            return res.warn('', req.__('PHONE_ALREADY_FOUND'));
        }

        const otp = await Otp.findOne({
            type: 'CHANGE_PHONE',
            countryCode,
            phone,
            validTill: {
                $gte: utcDateTime(),
            },
            isVerified: true,
        });

        if (!otp) {
            return res.warn('', req.__('PHONE_NOT_VERIFIED'));
        }

        user.countryCode = countryCode;
        user.phone = phone;
        await user.save();

        otp.validTill = null;
        await otp.save();

        return res.success('', req.__('PHONE_CHANGED'));
    }

    async notificationToggle(req, res) {
        const { user } = req;
        user.pushNotificationAllowed = !user.pushNotificationAllowed;
        await user.save();

        return res.success(
            {
                pushNotificationAllowed: user.pushNotificationAllowed,
            },
            user.pushNotificationAllowed ? req.__('NOTIFICATION_TURNED_ON') : req.__('NOTIFICATION_TURNED_OFF')
        );
    }
}

module.exports = new UserController();
