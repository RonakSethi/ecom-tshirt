const express = require('express');
const router = express.Router();
const UserController = require('./UserController');
const validations = require('./UserValidations');
const { validate } = require('../../util/validations');
const { verifyToken } = require('../../util/auth');

router.get('/list',
    verifyToken,
    UserController.list
);

router.get('/profile',
    verifyToken,
    validate(validations.optionalId, 'query'),
    UserController.profile,
);

router.put('/profile',
    verifyToken,
    validate(validations.updateProfile),
    UserController.updateProfile,
);

router.put('/password',
    verifyToken,
    validate(validations.updatePassword),
    UserController.updatePassword,
);

router.put('/email',
    verifyToken,
    validate(validations.updateEmail),
    UserController.updateEmail,
);

router.put('/phone',
    verifyToken,
    validate(validations.updatePhone),
    UserController.updatePhone,
);

router.get(
    '/notification-toggle',
    verifyToken,
    UserController.notificationToggle,
);

module.exports = router;