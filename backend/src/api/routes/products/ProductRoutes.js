const express = require('express');
const router = express.Router();
const ProductController = require('./ProductController');
const validations = require('./ProductValidations');
const { validate } = require('../../util/validations');
const { verifyToken, verifyTokenUserOrAdmin } = require('../../util/auth');

router.get('/:productId',  ProductController.getProductDetail);
router.put('/:productId', verifyTokenUserOrAdmin,  ProductController.updateProduct);
router.get('/', ProductController.list);
router.post('/', verifyTokenUserOrAdmin,  ProductController.add);

module.exports = router;
