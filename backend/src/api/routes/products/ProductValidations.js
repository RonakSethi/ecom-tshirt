const { Joi, common } = require('../../util/validations');

const addProduct = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    sku: Joi.string()
        .trim()
        .required(),
    parentId: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/) // Validate as valid ObjectId format
        .optional()
        .allow(''),
    categoryId: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/) // Validate as valid ObjectId format
        .optional()
        .allow(''),
    description: Joi.string()
        .trim()
        .allow(''),
    attributes: Joi.object({
        grossWt: Joi.number()
            .optional()
            .allow(''),
        netWt: Joi.number()
            .optional()
            .allow(''),
        diaWt: Joi.number()
            .optional()
            .allow(''),
        stoneWt: Joi.number()
            .optional()
            .allow(''),
        MetalAndTechnique: Joi.string()
            .optional()
            .allow(''),
        Dimesnion: Joi.string()
            .optional()
            .allow(''),
        CountryOfOrigin: Joi.string()
            .optional()
            .allow(''),
        StoneUsed: Joi.string()
            .optional()
            .allow(''),
        hallmarked: Joi.string()
            .optional()
            .allow(''),
    }).optional(),
    isSize: Joi.boolean().default(false),
    sizeType: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/) // Validate as valid ObjectId format
        .optional()
        .allow(''),
    tags: Joi.string().allow(''),
    originalPrice: Joi.number().optional(),
    discountPrice: Joi.number().optional(),
    stock: Joi.number().default(0),
    images: Joi.array()
        .items(
            Joi.object({
                public_id: Joi.string().required(),
                url: Joi.string().required(),
            })
        )
        .required(),
    reviews: Joi.array()
        .items(
            Joi.object({
                user: Joi.object(),
                rating: Joi.number(),
                comment: Joi.string(),
                productId: Joi.string(),
                createdAt: Joi.date().default(Date.now, 'Date of review creation'),
            })
        )
        .optional()
        .allow(''),
    ratings: Joi.number()
        .optional()
        .allow(''),
    shopId: Joi.string()
        .required()
        .regex(/^[0-9a-fA-F]{24}$/), // Validate as valid ObjectId format
    shop: Joi.object().required(),
    sold_out: Joi.number().default(0),
    isSuspended: Joi.boolean().default(false),
    isDeleted: Joi.boolean().default(false),
});

module.exports = { addProduct };