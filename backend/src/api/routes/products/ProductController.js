const {
    models: { Product },
} = require('../../../../lib/models');
const { utcDateTime } = require('../../../../lib/util');
const slugify = require('slugify');

class ProductController {
    async list(req, res) {
        const {
            search,
            catId,
            sku,
            type,
            brand,
            occasion,
            fabric,
            length,
            pattern,
            color,
            tags,
            sizeType,
            minPrice,
            maxPrice,
            page = 1,
            limit = 10,
        } = req.query;
        const finalResponse = {
            items: [],
            count: 0,
            page,
            limit,
        };
        // Convert page and limit to numbers
        const pageNumber = parseInt(page);
        const limitNumber = parseInt(limit);

        // Calculate the number of items to skip
        let skip = (pageNumber - 1) * limitNumber;
        try {
            let qry = { isDeleted: false };
            if (catId) {
                qry.catId = catId;
            }
            if (sku) {
                qry.sku = sku;
            }
            if (type) {
                qry.type = type;
            }
            if (search) {
                //add a condition for seach data by title
                qry.name = { $regex: new RegExp(search, 'i') }; // Case-insensitive search
            }

            // Apply price range query if minPrice and maxPrice are provided
            if (minPrice && maxPrice) {
                qry['prices.b2cPrice.sellingPrice'] = { $gte: minPrice, $lte: maxPrice };
            } else if (minPrice) {
                qry['prices.b2cPrice.sellingPrice'] = { $gte: minPrice };
            } else if (maxPrice) {
                qry['prices.b2cPrice.sellingPrice'] = { $lte: maxPrice };
            }

            if (brand) qry['attributes.brand'] = brand;
            if (occasion) qry['attributes.occasion'] = occasion;
            if (fabric) qry['attributes.fabric'] = fabric;
            if (length) qry['attributes.length'] = length;
            if (pattern) qry['attributes.pattern'] = pattern;
            if (color) qry['attributes.color.name'] = { $in: color.split(',') }; // Assuming color is an array field, split string into array
            if (tags) qry['attributes.tags'] = { $in: tags.split(',') }; // Assuming tags is an array field, split string into array
            if (sizeType) qry['sizeType'] = { $in: sizeType.split(',') }; // Assuming sizeType is an array field, split string into array

            const productCount = await Product.count(qry);

            // Fetch all products from the database and populate the subject and classId fields
            const products = await Product.find(qry)
                .sort({ created: -1 })
                .skip(skip)
                .limit(limitNumber)
                .populate({
                    path: 'categoryId',
                    select: 'parentId name slug image'
                })
                .exec();

            // Check if there are any questions
            if (products.length === 0) {
                return res.warn('', req.__('NO_PRODUCT_FOUND'));
            }

            finalResponse.items = products;
            finalResponse.count = productCount;

            // Respond with the list of questions, including populated subject and classId fields
            res.success(finalResponse);
        } catch (error) {
            // Handle errors
            console.log('Error fetching products:', error);
            res.warn(error);
        }
    }

    async add(req, res) {
        let {
            sku,
            name,
            categoryId,
            description,
            additionalInformation,
            type,
            attributes,
            isSize,
            sizeType,
            seo,
            b2cPrice,
            b2bPrice,
            stock,
            mainImage,
            images,
            shopId,
            shop,
            sold_out,
            isSuspended,
            isDeleted,
        } = req.body;

        try {
            const productData = {
                sku,
                name,
                categoryId,
                description,
                additionalInformation,
                type,
                attributes,
                isSize,
                sizeType,
                seo,
                prices: {
                    b2cPrice,
                    b2bPrice,
                },
                stock,
                mainImage,
                images,
                shopId,
                shop,
                sold_out,
                isSuspended,
                isDeleted,
            };

            const product = new Product(productData);
            const newProduct = await product.save();

            return res.success(newProduct, req.__('PRODUCT_ADDED'));
        } catch (error) {
            console.log('Error while adding product:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }

    async getProductDetail(req, res) {
        try {
            const { productId } = req.params;
    
            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(productId && { _id: productId }), // Include _id in query if provided
            };
    
            const product = await Product.findOne(query)
                .populate({
                    path: 'categoryId',
                    select: 'parentId name slug image'
                });
    
            if (!product) {
                return res.warn({ error: 'Product not found' });
            }
    
            const category = product.categoryId;
    
            let parentCategory = null;
            if (category && category.parentId) {
                parentCategory = await Category.findById(category.parentId).select('parentId name slug image');
            }
    
            const responseData = {
                ...product.toObject(),
                categoryId: {
                    parentId: category.parentId,
                    name: category.name,
                    slug: category.slug,
                    image: category.image,
                    parentCategory: parentCategory ? {
                        parentId: parentCategory.parentId,
                        name: parentCategory.name,
                        slug: parentCategory.slug,
                        image: parentCategory.image,
                    } : null
                }
            };
    
            return res.success(responseData);
        } catch (error) {
            console.error('Error retrieving Product detail:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async updateProduct(req, res) {
        const { productId } = req.params;
        let {
            name,
            categoryId,
            description,
            additionalInformation,
            type,
            attributes,
            isSize,
            sizeType,
            seo,
            b2cPrice,
            b2bPrice,
            stock,
            mainImage,
            images,
            sold_out,
            isSuspended,
            isDeleted,
        } = req.body;
    
        try {
            // Find the product by id
            const product = await Product.findById(productId);
    
            if (!product) {
                return res.notFound({}, req.__('PRODUCT_NOT_FOUND'));
            }
    
            // Update product fields
            product.name = name ?? product.name;
            product.categoryId = categoryId ?? product.categoryId;
            product.description = description ?? product.description;
            product.additionalInformation = additionalInformation ?? product.additionalInformation;
            product.type = type ?? product.type;
            product.attributes = attributes ?? product.attributes;
            product.isSize = isSize ?? product.isSize;
            product.sizeType = sizeType ?? product.sizeType;
            product.seo = seo ?? product.seo;
            product.prices.b2cPrice = b2cPrice ?? product.prices.b2cPrice;
            product.prices.b2bPrice = b2bPrice ?? product.prices.b2bPrice;
            product.stock = stock ?? product.stock;
            product.mainImage = mainImage ?? product.mainImage;
            product.images = images ?? product.images;
            product.sold_out = sold_out ?? product.sold_out;
            product.isSuspended = isSuspended ?? product.isSuspended;
            product.isDeleted = isDeleted ?? product.isDeleted;
    
            // Save the updated product
            const updatedProduct = await product.save();
    
            return res.success(updatedProduct, req.__('PRODUCT_UPDATED'));
        } catch (error) {
            console.log('Error while updating product:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }
    
}
module.exports = new ProductController();
