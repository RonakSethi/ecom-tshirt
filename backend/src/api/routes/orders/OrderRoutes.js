const express = require('express');
const router = express.Router();
const OrderController = require('./OrderController');
const validations = require('./OrderValidations');
const { validate } = require('../../util/validations');
const { verifyToken, verifyTokenUserOrAdmin, verifyTokenAdmin } = require('../../util/auth');

router.get('/my-orders', verifyTokenUserOrAdmin, OrderController.MyOrdersList);
router.get('/all-orders', verifyTokenAdmin, OrderController.AllOrdersList);
router.post('/place-order', verifyTokenUserOrAdmin, OrderController.placeOrder);

module.exports = router;
