const {
    models: { Product, Cart, Order },
} = require('../../../../lib/models');
const { generateUniqueOrderId } = require('../../util/common');

class OrderController {
    async placeOrder(req, res) {
        const userId = req.user._id;
        const { products, paymentMethod, shippingAddress } = req.body;

        try {
            // Validate products
            if (!products || products.length === 0) {
                return res.warn('error', req.__('PLEASE PROVIDE PRODUCTS'));
            }
            // // Find the product to get its price
            // const product = await Product.findById(productId);
            // if (!product) {
            //     return res.notFound({}, req.__('PRODUCT_NOT_FOUND'));
            // }

            // Calculate total price
            let totalGPrice = 0;
            let totalPrice = 0;
            for (const item of products) {
                const product = await Product.findById(item.productId._id);
                if (!product) {
                    return res.notFound({}, req.__('PRODUCT_NOT_FOUND'));
                }
                totalGPrice = product.prices.b2cPrice.originalPrice * item.quantity;
                totalPrice += product.prices.b2cPrice.sellingPrice * item.quantity;
            }

            const order = new Order({
                user: userId,
                orderId: generateUniqueOrderId(),
                products,
                paymentMethod,
                shippingAddress,
                totalGPrice,
                totalPrice,
                discount: totalGPrice - totalPrice,
            });

            const savedOrder = await order.save();

            // Clear the user's cart
            await Cart.findOneAndUpdate({ userId }, { products: [], totalPrice: 0, gTotalPrice: 0, totalDiscount: 0 });

            return res.success(savedOrder, req.__('PRODUCT_ADDED'));
        } catch (error) {
            console.log('Error while Placing order:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }

    async MyOrdersList(req, res) {
        const userId = req.user._id;
        const { page = 1, limit = 10, status, paymentMethod } = req.query;

        // Convert page and limit to numbers
        const pageNumber = parseInt(page);
        const limitNumber = parseInt(limit);

        // Calculate the number of items to skip
        let skip = (pageNumber - 1) * limitNumber;

        try {
            let qry = { user: userId };

            if (status) {
                qry.status = status;
            }

            if (paymentMethod) {
                qry.paymentMethod = paymentMethod;
            }

            const orderCount = await Order.countDocuments(qry);

            const orders = await Order.find(qry)
                .sort({ createdAt: -1 })
                .skip(skip)
                .limit(limitNumber)
                .populate('products.productId', 'name prices mainImage')
                .exec();

            const finalResponse = {
                items: orders,
                count: orderCount,
                page: pageNumber,
                limit: limitNumber,
            };

            if (!orders || orders.length === 0) {
                return res.warn('error', req.__('NO_ORDERS_FOUND'));
            }

            return res.success(finalResponse, req.__('ORDERS_FETCHED_SUCCESSFULLY'));
        } catch (error) {
            console.log('Error fetching orders:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }

    async AllOrdersList(req, res) {
        const userId = req.user._id;
        const { page = 1, limit = 10, status, paymentMethod, minTotalPrice, maxTotalPrice } = req.query;

        // Convert page and limit to numbers
        const pageNumber = parseInt(page);
        const limitNumber = parseInt(limit);

        // Calculate the number of items to skip
        let skip = (pageNumber - 1) * limitNumber;

        try {
            let qry = {};

            if (status) {
                qry.status = status;
            }

            if (paymentMethod) {
                qry.paymentMethod = paymentMethod;
            }

            if (minTotalPrice && maxTotalPrice) {
                qry.totalPrice = { $gte: minTotalPrice, $lte: maxTotalPrice };
            } else if (minTotalPrice) {
                qry.totalPrice = { $gte: minTotalPrice };
            } else if (maxTotalPrice) {
                qry.totalPrice = { $lte: maxTotalPrice };
            }

            const orderCount = await Order.countDocuments(qry);

            const orders = await Order.find(qry)
                .sort({ createdAt: -1 })
                .skip(skip)
                .limit(limitNumber)
                .populate('products.productId', 'name prices mainImage')
                .exec();

            const finalResponse = {
                items: orders,
                count: orderCount,
                page: pageNumber,
                limit: limitNumber,
            };

            if (!orders || orders.length === 0) {
                return res.warn('error', req.__('NO_ORDERS_FOUND'));
            }

            return res.success(finalResponse, req.__('ORDERS_FETCHED_SUCCESSFULLY'));
        } catch (error) {
            console.log('Error fetching orders:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }
}
module.exports = new OrderController();
