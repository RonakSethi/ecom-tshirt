const {
    models: { Category },
} = require('../../../../lib/models');
const { utcDateTime } = require('../../../../lib/util');
const slugify = require('slugify');

class CategoryController {
    async list(req, res) {
        try {
            let { catId } = req.query;
            let query = {
                isDeleted: false,
                isSuspended: false,
            };

            if (!catId) {
                query.parentId = null;
            } else if (catId !== 'tree') {
                query.parentId = catId;
            }

            let categories = await Category.find(query);

            if (catId === 'tree') {
                categories = await categoryTree(categories);
            }

            return res.success(categories);
        } catch (error) {
            console.error('Error retrieving categories:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async add(req, res) {
        let { name, image, description, parentId } = req.body;
        try {
            const catData = {
                name,
                slug: slugify(name, { remove: undefined, lower: true }),
                image,
                description,
                parentId,
            };

            const category = new Category(catData);
            const newCat = await category.save();
            // Update hasChild parameter of parent category if parentId is provided
            if (parentId) {
                await Category.findByIdAndUpdate(parentId, { $set: { hasChild: true } });
            }

            return res.success({ ...newCat._doc }, req.__('CATEGORY_ADDED'));
        } catch (e) {
            console.log('e >>', e);
            if (e.code === 11000) {
                return res.badRequest({}, req.__('CATEGORY_ALREADY_EXISTS'));
            } else {
                return res.badRequest({}, req.__(e.message));
            }
        }
    }
}

const categoryTree = (categories, parentId = null) => {
    const categoryList = [];
    let category;

    if (parentId === null) {
        category = categories.filter(cat => cat.parentId == undefined);
    } else {
        category = categories.filter(
            cat =>
                cat.parentId !== undefined && parentId !== undefined && cat.parentId.toString() == parentId.toString()
        );
    }

    for (let cate of category) {
        categoryList.push({
            _id: cate._id,
            name: cate.name,
            image: cate.image,
            slug: cate.slug,
            description: cate.description,
            parentId: cate.parentId,
            hasChild: cate.hasChild,
            created: cate.created,
            updated: cate.updated,

            children: categoryTree(categories, cate._id),
        });
    }

    return categoryList;
};

module.exports = new CategoryController();
