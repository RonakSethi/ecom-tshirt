const { Joi, common } = require('../../util/validations');

const addCategory = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    image: Joi.string()
        .trim()
        .max(500)
        .optional(),
    description: Joi.string()
        .trim()
        .min(3)
        .optional()
        .allow(''),
    parentId: Joi.objectId()
        .valid()
        .optional()
        .allow(''),
});

module.exports = { addCategory };
