const express = require('express');
const router = express.Router();
const CategoryController = require('./CategoryController');
const validations = require('./CategoryValidations');
const { validate } = require('../../util/validations');
const { verifyToken, verifyTokenUserOrAdmin } = require('../../util/auth');

router.get('/', CategoryController.list);
router.post('/', verifyTokenUserOrAdmin, validate(validations.addCategory), CategoryController.add);

module.exports = router;
