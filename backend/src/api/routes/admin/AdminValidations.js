const { Joi, common } = require('../../util/validations');
const { languages } = require('../../../../lib/i18n');

const logIn = Joi.object().keys({
    email: common.email,
    password: Joi.string().required(),
});

const forgotPassword = Joi.object().keys({
    email: common.email,
});

const resetPassword = Joi.object().keys({
    otp: Joi.string().required(),
    newPassword: common.adminPassword,
});

const updatePassword = Joi.object().keys({
    currentPassword: Joi.string().required(),
    newPassword: common.adminPassword,
    confirmPassword: Joi.string()
        .required()
        .valid(Joi.ref('newPassword'))
        .error(([error]) => {
            const { locale } = error.options;
            const language = languages[locale];
            return {
                message: language.validation.custom.sameAs(error.context.key, 'newPassword'),
            };
        }),
});

const isEmailExists = Joi.object().keys({
    email: common.email,
    id: Joi.objectId()
        .valid()
        .optional(),
});

const settings = Joi.object().keys({
    androidAppVersion: Joi.string()
        .regex(/^[\d]+\.[\d]+\.[\d]+$/, 'Semantic Version')
        .required(),
    androidForceUpdate: Joi.boolean().required(),
    iosAppVersion: Joi.string()
        .regex(/^[\d]+\.[\d]+\.[\d]+$/, 'Semantic Version')
        .required(),
    iosForceUpdate: Joi.boolean().required(),
    maintenance: Joi.boolean().required(),
});

const activeInactive = Joi.object().keys({
    modelName: Joi.string().required(),
    status: Joi.boolean().required(),
    modelId: Joi.objectId()
        .valid()
        .required(),
});

module.exports = {
    logIn,
    updatePassword,
    forgotPassword,
    resetPassword,
    isEmailExists,
    settings,
    activeInactive,
};
