const {
    models: { Admin, AdminSettings, User, Product, SizeType,
        CaptionTag,
        ClothLength,
        Color,
        Discount,
        Fabric,
        Sections,
        Occasion,
        Pattern,
        PaymentMode,
        Brand,
        Category, },
} = require('../../../../lib/models');
const { utcDateTime, generateResetToken, logError } = require('../../../../lib/util');
const { signToken } = require('../../util/auth');
const { sendMail } = require('../../../../lib/mailer');
const { getPlatform } = require('../../util/common');

class AdminController {
    async logIn(req, res) {
        const { email, password } = req.body;
        const admin = await Admin.findOne({ email, isDeleted: false });

        if (!admin) {
            return res.warn('error', req.__('INVALID_CREDENTIALS'));
        }
        if (admin.isSuspended) {
            return res.warn('error', req.__('YOUR_ACCOUNT_SUSPENDED'));
        }

        if (admin.failedLoginAttempts >= parseInt(process.env.allowedFailedLoginAttempts || 3)) {
            let difference = admin.preventLoginTill - utcDateTime().valueOf();
            if (difference > 0) {
                let differenceInSec = Math.abs(difference) / 1000;
                differenceInSec -= Math.floor(differenceInSec / 86400) * 86400;
                differenceInSec -= (Math.floor(differenceInSec / 3600) % 24) * 3600;
                const minutes = Math.floor(differenceInSec / 60) % 60;
                differenceInSec -= minutes * 60;
                const seconds = differenceInSec % 60;
                return res.warn(
                    'error',
                    req.__(
                        'LOGIN_DISABLED',
                        `${minutes ? `${minutes} ${req.__('KEY_MINUTES')} ` : ''}${seconds} ${req.__('KEY_SECONDS')}`
                    )
                );
            }
        }
        const passwordMatched = await admin.comparePassword(password);
        if (!passwordMatched) {
            admin.failedLoginAttempts = admin.failedLoginAttempts + 1;
            admin.preventLoginTill = utcDateTime(
                utcDateTime().valueOf() + parseInt(process.env.preventLoginOnFailedAttemptsTill || 5) * 60000
            ).valueOf();
            await admin.save();
            const chanceLeft = parseInt(process.env.allowedFailedLoginAttempts || 3) - admin.failedLoginAttempts;
            req.flash(
                'error',
                req.__(
                    'INVALID_CREDENTIALS_LIMIT',
                    `${
                        chanceLeft <= 0
                            ? `${req.__('KEY_LOGIN_DISABLED')}`
                            : `${req.__('KEY_YOU_HAVE_ONLY')} ${chanceLeft} ${req.__('KEY_CHANCE_LEFT')}`
                    }`
                )
            );
            return res.redirect('/auth/log-in');
        }

        admin.authTokenIssuedAt = utcDateTime().valueOf();
        admin.failedLoginAttempts = 0;
        admin.preventLoginTill = 0;
        await admin.save();

        const platform = getPlatform(req);
        const token = signToken(admin, platform);
        const userJson = admin.toJSON();
        ['password', 'authTokenIssuedAt', 'failedLoginAttempts', 'preventLoginTill', 'social', '__v'].forEach(
            key => delete userJson[key]
        );

        return res.success(
            {
                token,
                user: userJson,
            },
            req.__('LOGIN_SUCCESS')
        );
    }

    async logout(req, res) {
        req.session.user = null;
        req.session.token = null;
        req.flash('success', req.__('LOGOUT_SUCCESS'));
        return res.redirect('/auth/log-in');
    }

    async dashboard(req, res) {
        return res.render('index');
    }

    async profilePage(req, res) {
        const { user } = req;
        return res.success('profile', {
            user,
        });
    }

    async profile(req, res) {
        const { user } = req;
        const { firstName, lastName, email, countryCode, contactNumber } = req.body;
        user.firstName = firstName;
        user.lastName = lastName;
        user.email = email;
        user.countryCode = countryCode;
        user.contactNumber = contactNumber;
        await user.save();
        res.warn('success', req.__('PROFILE_UPDATED'));
        return res.redirect('/profile');
    }

    async activeInactive(req, res) {
        const { modelName, status, modelId } = req.body;

        try {
            const models = {
                User,
                Product,
                SizeType,
                CaptionTag,
                ClothLength,
                Color,
                Discount,
                Fabric,
                Sections,
                Occasion,
                Pattern,
                PaymentMode,
                Brand,
                Category,
            };

            if (!models[modelName]) {
                return res.warn('error', req.__('INVALID_MODEL'));
            }

            const updateStatus = await models[modelName].updateOne({ _id: modelId }, { isDeleted: status });

            if (updateStatus.nModified === 0) {
                return res.warn('error', req.__('RECORD_NOT_FOUND'));
            }

            return res.success({}, req.__('DELETE_SUCCESS'));
        } catch (error) {
            console.error('Error updating model:', error);
            return res.error(error, 'An error occurred while updating the model');
        }
    }

    async changePasswordPage(req, res) {
        return res.render('change-password');
    }

    async changePassword(req, res) {
        const { user } = req;
        const { currentPassword, newPassword } = req.body;

        const passwordMatched = await user.comparePassword(currentPassword);
        if (!passwordMatched) {
            req.flash('error', req.__('PASSWORD_MATCH_FAILURE'));
            return res.redirect('/change-password');
        }

        user.password = newPassword;
        await user.save();

        req.flash('success', req.__('PASSWORD_CHANGED'));
        return res.redirect('/change-password');
    }

    async forgotPasswordPage(req, res) {
        if (req.session.user) {
            return res.redirect('/');
        }
        return res.render('forgot-password');
    }

    async forgotPassword(req, res) {
        if (req.session.user) {
            return res.redirect('/');
        }
        const { email } = req.body;

        const admin = await Admin.findOne({
            email,
            isDeleted: false,
        });

        if (!admin) {
            req.flash('error', req.__('USER_NOT_FOUND'));
            return res.redirect('/auth/forgot-password');
        }

        if (admin.isSuspended) {
            req.flash('error', req.__('YOUR_ACCOUNT_SUSPENDED'));
            return res.redirect('/auth/forgot-password');
        }

        admin.resetToken = generateResetToken();
        await admin.save();

        req.flash('success', req.__('FORGOT_PASSWORD_MAIL_SUCCESS'));
        res.redirect('/auth/log-in');

        sendMail('admin-forgot-password', req.__('EMAIL_RESET_PASSWORD'), email, {
            verification_code: admin.resetToken,
            resetLink: `${process.env.SITE_URL}/auth/reset-password?email=${email}`,
        }).catch(error => {
            logError(`Failed to send password reset link to ${email}`);
            logError(error);
        });
    }

    async resetPasswordPage(req, res) {
        if (req.session.user) {
            return res.redirect('/');
        }
        const { email } = req.query;

        if (!email) {
            req.flash('error', req.__('INVALID_RESET_PASS_REQUEST'));
            return res.redirect('/auth/forgot-password');
        }

        const admin = await Admin.findOne({
            email,
            isDeleted: false,
        });

        if (!admin || !admin.resetToken) {
            req.flash('error', req.__('INVALID_RESET_PASS_REQUEST'));
            return res.redirect('/auth/forgot-password');
        }

        if (admin.isSuspended) {
            req.flash('error', req.__('YOUR_ACCOUNT_SUSPENDED'));
            return res.redirect('/auth/forgot-password');
        }

        return res.render('reset-password');
    }

    async resetPassword(req, res) {
        if (req.session.user) {
            return res.redirect('/');
        }
        const { email } = req.query;
        const { otp, newPassword } = req.body;

        if (!email) {
            req.flash('error', req.__('INVALID_RESET_PASS_REQUEST'));
            return res.redirect('/auth/forgot-password');
        }

        const admin = await Admin.findOne({
            email,
            isDeleted: false,
        });

        if (!admin) {
            req.flash('error', req.__('USER_NOT_FOUND'));
            return res.redirect('/auth/forgot-password');
        }

        if (admin.isSuspended) {
            req.flash('error', req.__('YOUR_ACCOUNT_SUSPENDED'));
            return res.redirect('/auth/forgot-password');
        }

        if (!(admin.resetToken === otp)) {
            req.flash('error', req.__('INVALID_OTP'));
            return res.redirect(req.headers['referer']);
        }

        admin.password = newPassword;
        admin.resetToken = null;
        await admin.save();

        req.flash('success', req.__('PASSWORD_CHANGED'));
        return res.redirect('/auth/log-in');
    }

    async isEmailExists(req, res) {
        const { email, id } = req.body;
        const matchCond = {
            isDeleted: false,
            email,
        };
        id &&
            (matchCond._id = {
                $ne: id,
            });
        const count = await Admin.countDocuments(matchCond);

        return res.send(count === 0);
    }

    async settingsPage(req, res) {
        let settings = await AdminSettings.findOne({}).lean();

        return res.render('setting', {
            settings,
        });
    }

    async updateSettings(req, res) {
        const { androidAppVersion, androidForceUpdate, iosAppVersion, iosForceUpdate, maintenance } = req.body;

        await AdminSettings.updateMany(
            {},
            {
                $set: {
                    androidAppVersion,
                    androidForceUpdate,
                    iosAppVersion,
                    iosForceUpdate,
                    maintenance,
                },
            },
            {
                upsert: true,
            }
        );

        req.flash('success', req.__('SETTINGS_UPDATE_SUCCESS'));
        return res.redirect('/');
    }
    

    async sslcheck(req, res){
        const axios = require('axios');
        try {
            const { hostname } = req.query;
            const response = await axios.get(`https://api.ssllabs.com/api/v3/analyze?host=${hostname}&all=on`);
            console.log("response >>>",JSON.stringify(response.data));
            res.json(response.data);
          } catch (error) {
            res.status(500).json({ error: error.message });
          }
    }

    async test(req, res){
        const axios = require('axios');
        const { hostname } = req.query;
  if (!hostname) {
    return res.status(400).json({ error: 'Domain parameter is required' });
  }

  try {
    const response = await fetch(`https://api.whois/v1/whois?domain=${hostname}`);
    console.log("response  >>",response)
    if (!response.ok) {
      throw new Error('Failed to fetch domain information');
    }
    const domainInfo = await response.json();
    const creationDate = new Date(domainInfo.creationDate);
    const currentDate = new Date();
    const ageInMilliseconds = currentDate - creationDate;
    const ageInYears = ageInMilliseconds / (1000 * 60 * 60 * 24 * 365);
    res.json({ domainAge: ageInYears.toFixed(2) });
  } catch (error) {
    console.log("error >>",error)
    res.status(500).json({ error: error.message });
  }
    }
}

module.exports = new AdminController();
