const express = require('express');
const router = express.Router();
const validations = require('./AdminValidations');
const AdminController = require('./AdminController');
const { validate } = require('../../util/validations');

router.post('/log-in', validate(validations.logIn), AdminController.logIn);
router.get('/log-out', AdminController.logout);
router.post('/forgot-password', validate(validations.forgotPassword), AdminController.forgotPassword);
router.post('/reset-password', validate(validations.resetPassword), AdminController.resetPassword);
router.delete('/delete-row', validate(validations.activeInactive), AdminController.activeInactive);
router.get('/test', AdminController.test);


module.exports = router;
