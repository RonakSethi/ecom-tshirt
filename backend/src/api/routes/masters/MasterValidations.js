const { Joi, common } = require('../../util/validations');

const addSize = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    values: Joi.array()
        .items(
            Joi.string()
                .trim()
                .max(500)
        )
        .optional(),
});

const addCaptionTag = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addClothLength = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addColor = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    hexCode: Joi.string()
        .trim()
        .optional()
        .allow(''),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addDiscount = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    value: Joi.number().required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addFabric = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    image: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addOccasions = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    image: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addPattern = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    image: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addSections = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

const addPaymentMode = Joi.object().keys({
    name: Joi.string()
        .trim()
        .required(),
    sortOrder: Joi.number()
        .optional()
        .allow(''),
});

module.exports = {
    addSize,
    addCaptionTag,
    addClothLength,
    addColor,
    addDiscount,
    addFabric,
    addSections,
    addOccasions,
    addPattern,
    addPaymentMode
};
