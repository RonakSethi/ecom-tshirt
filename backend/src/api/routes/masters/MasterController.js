const {
    models: {
        SizeType,
        CaptionTag,
        ClothLength,
        Color,
        Discount,
        Fabric,
        Sections,
        Occasion,
        Pattern,
        PaymentMode,
        Brand,
    },
} = require('../../../../lib/models');

class MasterController {
    // APIs for size
    async getSize(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await SizeType.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving size type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }
    async addSize(req, res) {
        const { name, values } = req.body;
        try {
            // Create and save the new size type to the database
            const addedData = await SizeType.create({ name, values });

            // Return success response with the added size type
            return res.success({ ...addedData._doc }, req.__('SIZE_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding size type:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for caption tag
    async getCaptionTag(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await CaptionTag.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving size type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }
    async addCaptionTag(req, res) {
        const { name, sortOrder } = req.body;
        try {
            // Create and save the new size type to the database
            const addedData = await CaptionTag.create({ name, sortOrder });
            // Return success response with the added size type
            return res.success({ ...addedData._doc }, req.__('CAPTION_TAG_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding caption tag:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for cloth length
    async getClothLength(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await ClothLength.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving size type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }
    async addClothLength(req, res) {
        const { name, sortOrder } = req.body;
        try {
            // Create and save the new cloth length to the database
            const addedData = await ClothLength.create({ name, sortOrder });
            // Return success response with the added size type
            return res.success({ ...addedData._doc }, req.__('CAPTION_TAG_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding caption tag:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for colors
    async getColors(req, res) {
        try {
            const { dataId, name, hexCode } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
                ...(hexCode && { hexCode }), // Include hexCode in query if provided
            };

            const data = await Color.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving size type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addColor(req, res) {
        const { name, hexCode, sortOrder } = req.body;
        try {
            const addedData = await Color.create({ name, hexCode, sortOrder });

            return res.success({ ...addedData._doc }, req.__('COLOR_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding color:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for Discount
    async getDiscountList(req, res) {
        try {
            const { dataId, name, value } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
                ...(value && { value }), // Include hexCode in query if provided
            };

            const data = await Discount.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving size type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addDiscount(req, res) {
        const { name, value, sortOrder } = req.body;
        try {
            const addedData = await Discount.create({ name, value, sortOrder });

            return res.success({ ...addedData._doc }, req.__('DISCOUNT_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding discount:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for Fabric
    async getFabrics(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await Fabric.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving fabric type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addFabric(req, res) {
        const { name, image, sortOrder } = req.body;
        try {
            const addedData = await Fabric.create({ name, image, sortOrder });

            return res.success({ ...addedData._doc }, req.__('DISCOUNT_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding fabric:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for Sections
    async getSections(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await Sections.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving sections type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addSections(req, res) {
        const { name, sortOrder } = req.body;
        try {
            const addedData = await Sections.create({ name, sortOrder });

            return res.success({ ...addedData._doc }, req.__('SECTION_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding section:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for Occasions
    async getOccasions(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await Occasion.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving Occasion type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addOccasions(req, res) {
        const { name, image, sortOrder } = req.body;
        try {
            const addedData = await Occasion.create({ name, image, sortOrder });

            return res.success({ ...addedData._doc }, req.__('DISCOUNT_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding Occasion:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for Pattern
    async getPatterns(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await Pattern.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving Pattern type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addPattern(req, res) {
        const { name, image, sortOrder } = req.body;
        try {
            const addedData = await Pattern.create({ name, image, sortOrder });

            return res.success({ ...addedData._doc }, req.__('PATTERN_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding Pattern:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for Pattern
    async getBrands(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await Brand.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving Brand type:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }

    async addBrand(req, res) {
        const { name, image, sortOrder } = req.body;
        try {
            const addedData = await Brand.create({ name, image, sortOrder });

            return res.success({ ...addedData._doc }, req.__('BRAND_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding Brand:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }

    // APIs for cloth length
    async getPaymentMode(req, res) {
        try {
            const { dataId, name } = req.query;

            const query = {
                isDeleted: false,
                isSuspended: false,
                ...(dataId && { _id: dataId }), // Include _id in query if provided
                ...(name && { name }), // Include name in query if provided
            };

            const data = await PaymentMode.find(query);

            return res.success(data);
        } catch (error) {
            console.error('Error retrieving Payment Mode:', error);
            return res.warn({ error: 'Internal server error' });
        }
    }
    async addPaymentMode(req, res) {
        const { name, sortOrder } = req.body;
        try {
            // Create and save the new cloth length to the database
            const addedData = await PaymentMode.create({ name, sortOrder });
            // Return success response with the added size type
            return res.success({ ...addedData._doc }, req.__('PAYMENT_MODE_ADDED'));
        } catch (error) {
            // Return bad request response if an error occurs
            console.error('Error adding Payment Mode:', error);
            return res.badRequest({}, req.__(error.message));
        }
    }
}

module.exports = new MasterController();
