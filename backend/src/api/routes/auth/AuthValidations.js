const { Joi, common } = require('../../util/validations');
const {
    enums: {
        OtpType,
    },
} = require('../../../../lib/models');

const requestOtp = Joi.object().keys({
    type: Joi.string()
        .valid(...Object.keys(OtpType))
        .required(),
    countryCode: common.countryCode,
    phone: common.phone,
});

const verifyOtp = requestOtp.keys({
    token: common.otp,
});

const signUp = Joi.object().keys({
    fullName: Joi.string()
        .trim()
        .min(3)
        .max(30)
        .required(),
    age: common.age,
    email: common.email
        .optional(),
    countryCode: common.countryCode,
    phone: common.phone,
    password: common.password,
    deviceToken: Joi.string()
        .trim()
        .optional(),
});

const logIn = Joi.object().keys({
    countryCode: common.countryCode,
    phone: common.phone,
    password: Joi.string()
        .trim()
        .required(),
    deviceToken: Joi.string()
        .trim()
        .optional(),
});

const resetPassword = Joi.object().keys({
    countryCode: common.countryCode,
    phone: common.phone,
    password: common.password,
});

module.exports = {
    requestOtp,
    verifyOtp,
    signUp,
    logIn,
    resetPassword,
};
