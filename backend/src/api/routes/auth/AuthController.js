const {
    models: { Otp, User },
} = require('../../../../lib/models');
const { utcDateTime } = require('../../../../lib/util');
const { signToken } = require('../../util/auth');
const { getPlatform } = require('../../util/common');

class AuthController {
    async requestOtp(req, res) {
        const { type, countryCode, phone } = req.body;

        if (['SIGN_UP', 'CHANGE_PHONE'].indexOf(type) !== -1) {
            const user = await User.findOne({
                countryCode,
                phone,
                isDeleted: false,
            });

            if (user) {
                return res.warn('', req.__('PHONE_ALREADY_FOUND'));
            }
        }

        if (type === 'FORGOT_PASSWORD') {
            const user = await User.findOne({
                countryCode,
                phone,
                isDeleted: false,
            });

            if (!user) {
                return res.warn('', req.__('USER_NOT_FOUND'));
            }

            if (user.isSuspended) {
                return res.warn('', req.__('YOUR_ACCOUNT_SUSPENDED'));
            }
        }

        let token = '1234';
        const otp = await Otp.findOne({
            countryCode,
            phone,
        }) || new Otp({});

        otp.type = type;
        otp.countryCode = countryCode;
        otp.phone = phone;
        otp.token = token;
        otp.validTill = utcDateTime(utcDateTime().valueOf() + parseInt(process.env.otpValidMinutes || 5) * 60000);
        otp.isVerified = false;
        await otp.save();

        if (process.env.NODE_ENV === 'production') {
            // TODO: update token in production environment
        }

        await otp.save();
        return res.success('', req.__('OTP_SENT'));
    }

    async verifyOtp(req, res) {
        const { type, countryCode, phone, token } = req.body;

        if (['SIGN_UP', 'CHANGE_PHONE'].indexOf(type) !== -1) {
            const user = await User.findOne({
                countryCode,
                phone,
                isDeleted: false,
            });

            if (user) {
                return res.warn('', req.__('PHONE_ALREADY_FOUND'));
            }
        }

        if (type === 'FORGOT_PASSWORD') {
            const user = await User.findOne({
                countryCode,
                phone,
                isDeleted: false,
            });

            if (!user) {
                return res.warn('', req.__('USER_NOT_FOUND'));
            }

            if (user.isSuspended) {
                return res.warn('', req.__('YOUR_ACCOUNT_SUSPENDED'));
            }
        }

        const otp = await Otp.findOne({
            type,
            countryCode,
            phone,
            validTill: {
                $gte: utcDateTime(),
            },
            isVerified: false,
        });

        if (!otp || otp.token !== token) {
            return res.warn('', req.__('INVALID_OTP'));
        }

        otp.isVerified = true;
        await otp.save();

        return res.success('', req.__('OTP_VERIFIED'));
    }

    async signUp(req, res) {
        const {
            fullName,
            age,
            email,
            countryCode,
            phone,
            password,
            deviceToken
        } = req.body;

        const otp = await Otp.findOne({
            type: 'SIGN_UP',
            countryCode,
            phone,
            validTill: {
                $gte: utcDateTime(),
            },
            isVerified: true
        });

        if (!otp) {
            return res.warn('', req.__('PHONE_NOT_VERIFIED'));
        }

        const userMatchCond = {
            $or: [
                { $and: [{ countryCode }, { phone }] },
            ],
            isDeleted: false,
        };
        email && userMatchCond.$or.push({email});

        let user = await User.findOne(userMatchCond);

        if (user) {
            return res.warn('', req.__('USER_ALREADY_FOUND'));
        }

        const userData = {
            fullName,
            age,
            countryCode,
            phone,
            password
        };
        email && (userData.email = email);
        deviceToken && (userData.deviceToken = deviceToken);
        user = new User(userData);
        user.authTokenIssuedAt = utcDateTime().valueOf();
        await user.save();

        otp.validTill = null;
        await otp.save();

        const platform = getPlatform(req);
        const token = signToken(user, platform);
        const userJson = user.toJSON();
        ['password', 'authTokenIssuedAt', 'failedLoginAttempts', 'preventLoginTill', 'social', '__v'].forEach(key => delete userJson[key]);

        return res.success({
            token,
            user: userJson,
        }, req.__('LOGIN_SUCCESS'));
    }

    async logIn(req, res) {
        const { countryCode, phone, password, deviceToken } = req.body;
        let user = await User.findOne({
            countryCode,
            phone,
            isDeleted: false,
        });

        if (!user) {
            return res.warn('', req.__('USER_NOT_FOUND'));
        }

        if (user.isSuspended) {
            return res.warn('', req.__('YOUR_ACCOUNT_SUSPENDED'));
        }

        if (user.failedLoginAttempts >= parseInt(process.env.allowedFailedLoginAttempts || 3)) {
            let difference = user.preventLoginTill - utcDateTime().valueOf();
            if (difference > 0) {
                let differenceInSec = Math.abs(difference) / 1000;
                differenceInSec -= Math.floor(differenceInSec / 86400) * 86400;
                differenceInSec -= Math.floor(differenceInSec / 3600) % 24 * 3600;
                const minutes = Math.floor(differenceInSec / 60) % 60;
                differenceInSec -= minutes * 60;
                const seconds = differenceInSec % 60;
                return res.warn('', req.__('LOGIN_DISABLED', `${minutes ? `${minutes} ${req.__('KEY_MINUTES')} ` : ''}${seconds} ${req.__('KEY_SECONDS')}`));
            }
        }

        const passwordMatched = await user.comparePassword(password);
        if (!passwordMatched) {
            user.failedLoginAttempts = user.failedLoginAttempts + 1;
            user.preventLoginTill = utcDateTime(utcDateTime().valueOf() + parseInt(process.env.preventLoginOnFailedAttemptsTill || 5) * 60000).valueOf();
            await user.save();
            const chanceLeft = parseInt(process.env.allowedFailedLoginAttempts || 3) - user.failedLoginAttempts;
            return res.warn('', req.__('INVALID_CREDENTIALS_LIMIT', `${chanceLeft <= 0 ? `${req.__('KEY_LOGIN_DISABLED')}` : `${req.__('KEY_YOU_HAVE_ONLY')} ${chanceLeft} ${req.__('KEY_CHANCE_LEFT')}`}`));
        }

        deviceToken && (user.deviceToken = deviceToken);
        user.authTokenIssuedAt = utcDateTime().valueOf();
        user.failedLoginAttempts = 0;
        user.preventLoginTill = 0;
        await user.save();

        const platform = getPlatform(req);
        const token = signToken(user, platform);
        const userJson = user.toJSON();
        ['password', 'authTokenIssuedAt', 'failedLoginAttempts', 'preventLoginTill', 'social', '__v'].forEach(key => delete userJson[key]);

        return res.success({
            token,
            user: userJson,
        }, req.__('LOGIN_SUCCESS'));
    }

    async logout(req, res) {
        const { user } = req;
        user.authTokenIssuedAt = null;
        user.deviceToken = null;
        await user.save();
        return res.success('', req.__('LOGOUT_SUCCESS'));
    }

    async resetPassword(req, res) {
        const { countryCode, phone, password } = req.body;
        const otp = await Otp.findOne({
            type: 'FORGOT_PASSWORD',
            countryCode,
            phone,
            validTill: {
                $gte: utcDateTime(),
            },
            isVerified: true
        });

        if (!otp) {
            return res.warn('', req.__('PHONE_NOT_VERIFIED'));
        }

        let user = await User.findOne({
            countryCode,
            phone,
            isDeleted: false,
        });

        if (!user) {
            return res.warn('', req.__('USER_NOT_FOUND'));
        }

        if (user.isSuspended) {
            return res.warn('', req.__('YOUR_ACCOUNT_SUSPENDED'));
        }

        user.password = password;
        await user.save();

        otp.validTill = null;
        await otp.save();

        return res.success('', req.__('PASSWORD_CHANGED'));
    }
}

module.exports = new AuthController();