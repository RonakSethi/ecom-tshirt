const {
    models: { Product, Cart },
} = require('../../../../lib/models');

class CartController {
    // async add(req, res) {
    //     const userId = req?.user?._id;
    //     const { productId, quantity, selectedSize, selectedColor } = req.body;

    //     try {
    //         // Find the product to get its price
    //         const product = await Product.findById(productId);
    //         if (!product) {
    //             return res.notFound({}, req.__('PRODUCT_NOT_FOUND'));
    //         }

    //         // Calculate the price and total for the product
    //         const gPrice = product.prices.b2cPrice.originalPrice;
    //         const price = product.prices.b2cPrice.sellingPrice;
    //         const gTotal = gPrice * quantity;
    //         const total = price * quantity;
    //         const discount = gTotal - total;

    //         // Find the user's cart
    //         let cart = await Cart.findOne({ userId });

    //         // If the cart doesn't exist, create a new one
    //         if (!cart) {
    //             cart = new Cart({ userId, products: [], totalPrice: 0 });
    //         }

    //         // Check if the product already exists in the cart
    //         const productIndex = cart.products.findIndex(p => p.productId.toString() === productId);

    //         if (productIndex > -1) {
    //             // Update quantity and total for the existing product
    //             cart.products[productIndex].quantity += quantity;
    //             cart.products[productIndex].total += total;
    //             cart.products[productIndex].gTotal += gTotal;
    //             cart.products[productIndex].discount += discount;
    //         } else {
    //             // Add new product to the cart
    //             cart.products.push({
    //                 productId,
    //                 quantity,
    //                 selectedSize,
    //                 selectedColor,
    //                 price,
    //                 total,
    //                 gTotal,
    //                 discount
    //             });
    //         }

    //         // Update the total price of the cart
    //         cart.totalPrice += total;
    //         cart.gTotalPrice += gTotal;
    //         cart.totalDiscount += discount;

    //         // Save the cart
    //         const updatedCart = await cart.save();

    //         const cartDetail = await Cart.findOne({ userId }).populate('products.productId', 'name prices mainImage');

    //         return res.success(cartDetail, req.__('PRODUCT_ADDED'));
    //     } catch (error) {
    //         console.log('Error while adding product to cart:', error);
    //         return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
    //     }
    // }

    async add(req, res) {
        const userId = req?.user?._id;
        const { productId, quantity, selectedSize, selectedColor } = req.body;
    
        try {
            // Find the product to get its price
            const product = await Product.findById(productId);
            if (!product) {
                return res.status(404).json({ success: false, message: req.__('PRODUCT_NOT_FOUND') });
            }
    
            // Calculate the price and total for the product
            const gPrice = product.prices.b2cPrice.originalPrice;
            const price = product.prices.b2cPrice.sellingPrice;
            const gTotal = gPrice * quantity;
            const total = price * quantity;
            const discount = gTotal - total;
    
            // Find or create the user's cart
            let cart = await Cart.findOneAndUpdate(
                { userId },
                { $setOnInsert: { userId, products: [], totalPrice: 0, gTotalPrice: 0, totalDiscount: 0 } },
                { new: true, upsert: true }
            );
    
            // Check if the product already exists in the cart
            const productIndex = cart.products.findIndex(p => p.productId.toString() === productId);
    
            if (productIndex > -1) {
                // Update quantity and total for the existing product
                const existingProduct = cart.products[productIndex];
                const newQuantity = existingProduct.quantity + quantity;
                if (newQuantity <= 0) {
                    // Remove product if the new quantity is zero or negative
                    cart.products.splice(productIndex, 1);
                    cart.totalPrice -= existingProduct.total;
                    cart.gTotalPrice -= existingProduct.gTotal;
                    cart.totalDiscount -= existingProduct.discount;
                } else {
                    // Update the existing product's quantity and totals
                    const additionalTotal = price * quantity;
                    const additionalGTotal = gPrice * quantity;
                    const additionalDiscount = additionalGTotal - additionalTotal;
                    existingProduct.quantity = newQuantity;
                    existingProduct.total += additionalTotal;
                    existingProduct.gTotal += additionalGTotal;
                    existingProduct.discount += additionalDiscount;
                    cart.totalPrice += additionalTotal;
                    cart.gTotalPrice += additionalGTotal;
                    cart.totalDiscount += additionalDiscount;
                }
            } else {
                // Add new product to the cart if quantity is positive
                if (quantity > 0) {
                    cart.products.push({
                        productId,
                        quantity,
                        selectedSize,
                        selectedColor,
                        price,
                        total,
                        gTotal,
                        discount
                    });
                    cart.totalPrice += total;
                    cart.gTotalPrice += gTotal;
                    cart.totalDiscount += discount;
                } else {
                    return res.status(400).json({ success: false, message: req.__('INVALID_QUANTITY') });
                }
            }
    
            // Save the cart
            await cart.save();
    
            // Populate cart details for response
            const cartDetail = await Cart.findOne({ userId }).populate('products.productId', 'name prices mainImage');
    
            return res.status(200).json({ success: true, data: cartDetail, message: req.__('PRODUCT_ADDED') });
        } catch (error) {
            console.error('Error while adding product to cart:', error);
            return res.status(500).json({ success: false, message: req.__('INTERNAL_SERVER_ERROR') });
        }
    }

    async remove(req, res) {
        const userId = req?.user?._id;

        const { productId } = req.params;

        try {
            // Find the user's cart
            const cart = await Cart.findOne({ userId });

            if (!cart) {
                return res.notFound({}, req.__('CART_NOT_FOUND'));
            }
            if (productId === 'all') {
                // Empty the cart
                cart.products = [];
                cart.totalPrice = 0;
                cart.gTotalPrice = 0;
                cart.totalDiscount = 0;

                // Save the cart
                const updatedCart = await cart.save();

                return res.success(updatedCart, req.__('CART_EMPTIED'));
            } else {
                // Find the product in the cart
                const productIndex = cart.products.findIndex(p => p.productId.toString() === productId);

                if (productIndex === -1) {
                    return res.notFound({}, req.__('PRODUCT_NOT_FOUND_IN_CART'));
                }

                // Get the total of the product being removed
                const productTotal = cart.products[productIndex].total;
                const productgTotal = cart.products[productIndex].gTotal;
                const productDiscount = cart.products[productIndex].discount;

                // Remove the product from the cart
                cart.products.splice(productIndex, 1);

                // Update the total price of the cart
                cart.totalPrice -= productTotal;
                cart.gTotalPrice -= productgTotal;
                cart.totalDiscount -= productDiscount;

                // Save the cart
                const updatedCart = await cart.save();

                return res.success(updatedCart, req.__('PRODUCT_REMOVED'));
            }
        } catch (error) {
            console.log('Error while removing product from cart:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }

    async getCartDetails(req, res) {
        const userId = req?.user?._id;
        try {
            // Find the user's cart
            const cart = await Cart.findOne({ userId }).populate('products.productId', 'name prices mainImage');

            if (!cart) {
                return res.warn({}, req.__('CART_NOT_FOUND'));
            }

            return res.success(cart, req.__('CART_DETAILS_RETRIEVED'));
        } catch (error) {
            console.log('Error while retrieving cart details:', error);
            return res.serverError({}, req.__('INTERNAL_SERVER_ERROR'));
        }
    }
}
module.exports = new CartController();
