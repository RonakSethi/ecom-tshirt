const express = require('express');
const router = express.Router();
const CartController = require('./CartController');
const validations = require('./CartValidations');
const { validate } = require('../../util/validations');
const { verifyToken, verifyTokenUserOrAdmin } = require('../../util/auth');


router.delete('/:productId', verifyTokenUserOrAdmin,  CartController.remove);
router.get('/', verifyTokenUserOrAdmin, CartController.getCartDetails);
router.post('/', verifyTokenUserOrAdmin,  CartController.add);

module.exports = router;
