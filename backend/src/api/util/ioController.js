const validations = require('./iOValidations');
const { validateSocketData } = require('./validations');
const { verifyTokenSocket } = require('./auth');
const { withLanguage } = require('../../../lib/i18n');
const { consoleInDevEnv, utcDate, utcDateTime, showDate, randomString, logError } = require('../../../lib/util');
const { getObjectId } = require('./common');

module.exports.listen = function(server) {
    let io = require('socket.io')(server),
        loggedInUsers = {},
        loggedInUsersSockets = {};

    io.on('connection', function(socket) {
        // eslint-disable-next-line no-console
        consoleInDevEnv('a new user connected');

        socket.emit('connected', {});

        socket.on('user-login', async function(data, ack) {
            const { token, language } = data;
            const __ = withLanguage(language);

            const validationRes = await validateSocketData(validations.config, language, data);
            if (validationRes.isError) {
                return ack({
                    status: 'failed',
                    error: validationRes.msg,
                });
            }

            const verifyUserRes = await verifyTokenSocket(token, language);
            if (verifyUserRes.error) {
                return ack({
                    status: 'failed',
                    error: verifyUserRes.msg,
                });
            }

            loggedInUsers[verifyUserRes.data.user._id] = socket.id;
            loggedInUsersSockets[socket.id] = { user: verifyUserRes.data.user._id, socket };

            return ack({
                status: 'success',
            });
        });

        socket.on('user-logout', async function(data, ack) {
            const { token, language } = data;
            const __ = withLanguage(language);

            const validationRes = await validateSocketData(validations.config, language, data);
            if (validationRes.isError) {
                return ack({
                    status: 'failed',
                    error: validationRes.msg,
                });
            }

            const verifyUserRes = await verifyTokenSocket(token, language);
            if (verifyUserRes.error) {
                return ack({
                    status: 'failed',
                    error: verifyUserRes.msg,
                });
            }

            if (!(loggedInUsers[verifyUserRes.data.user._id])) {
                return ack({
                    status: 'failed',
                    error: __('UNAUTHORIZED'),
                });
            }

            delete loggedInUsersSockets[loggedInUsers[verifyUserRes.data.user._id]];
            delete loggedInUsers[verifyUserRes.data.user._id];
            return ack({
                status: 'success',
            });
        });

        socket.on('disconnect', async function() {
            let userLoginKey = null;
            for (let key in loggedInUsers) {
                if (loggedInUsers[key] === socket.id) {
                    userLoginKey = key;
                    break;
                }
            }

            userLoginKey && delete loggedInUsers[userLoginKey];
            delete loggedInUsersSockets[socket.id];

            // eslint-disable-next-line no-console
            consoleInDevEnv('a user disconnected');
        });
    });
};



