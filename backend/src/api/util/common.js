const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const { v4: uuidv4 } = require('uuid');

const getPlatform = req => req.headers['x-eshop-platform'];

const getLanguage = req => req.headers['accept-language'];

const generateToken = payload => jwt.sign(payload, process.env.JWT_SECRET);

const getObjectId = id => ObjectId(id);

const  generateUniqueOrderId = () => {
    // You can customize the prefix and format as needed
    const prefix = 'GC-';
    const timestamp = Date.now();
    const uniqueId = uuidv4();
    
    return `${prefix}${timestamp}-${uniqueId}`;
}

module.exports = {
    getPlatform,
    getLanguage,
    generateToken,
    getObjectId,
    generateUniqueOrderId
};
