const { Joi, common } = require('./validations');

const config = Joi.object().keys({
    language: Joi.string()
        .trim()
        .valid('en', 'ru', 'kk')
        .required(),
    token: Joi.string()
        .trim()
        .required(),
});

const requireId = Joi.object().keys({
    id: Joi.objectId()
        .valid()
        .required(),
});

const paginated = Joi.object().keys({
    page: common.page,
    perPage: common.perPage,
});

module.exports = {
    config,
    requireId,
    paginated,
};
